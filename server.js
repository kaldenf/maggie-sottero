var connect = require('connect');
var superstatic = require('superstatic');
var open = require('open')

var app = connect();

app.use(require('prerender-node').set('prerenderToken', 'EERH4fdnDhDhRGb8pG0V')); //maggie's key
//app.use(require('prerender-node').set('prerenderToken', '5E75clMWy4oVFiKWCOR7')); //kalden's key

app.use(superstatic({
  config: {
    public: "./dist",
    clean_urls: true,
    rewrites: [{
      "source":"**",
      "destination":"index.html"
    }]
  }
}));

app.listen(process.env.PORT || 8080);
open('http://localhost:8080')
