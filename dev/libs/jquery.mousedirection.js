/**
 * jQuery Mouse Direction Plugin
 * @version: 1.1
 * @author Hasin Hayder [hasin@leevio.com | http://hasin.me]
 */
(function ($) {
    var options = {};
    var oldx = 0;
    var oldy = 0;
    var direction="";
    $.mousedirection = function (opts) {
        var defaults = {
        };
        options = $.extend(defaults, opts);
        $(document).bind("mousemove", function (e) {
            var activeElement = e.target || e.srcElement;

			var top;
			var left;            
/*
            
            if (e.pageX > oldx && e.pageY > oldy) {
                direction="bottom-right";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX > oldx && e.pageY < oldy) {
                direction="top-right";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX < oldx && e.pageY < oldy) {
                direction="top-left";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX < oldx && e.pageY > oldy) {
                direction="bottom-left";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX > oldx && e.pageY == oldy) {
                direction="right";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX == oldx && e.pageY > oldy) {
                direction="down";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX == oldx && e.pageY < oldy) {
                direction="up";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
            else if (e.pageX < oldx && e.pageY == oldy) {
                direction="left";
                $(e.target).parent().scrollTop(e.pageY);
                $(e.target).parent().scrollLeft(e.pageX);
            }
*/
            $(activeElement).trigger(direction);
            $(activeElement).trigger({type:"mousedirection",direction:direction});
            oldx=e.pageX;
            oldy=e.pageY;
        });
    }
})(jQuery);