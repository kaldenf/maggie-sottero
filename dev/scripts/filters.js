app.filter('imagePathToMediumResImage', function() {
	return function(input) {
		var source = input.split("/");
		source = "Medium/" + source[1] + "";
		return source;
	}
})

app.filter('imagePathToLargeResImage', function() {
	return function(input) {
		var source = input.split("/");
		source = "Large/" + source[1] + "";
		return source;
	}
})

app.filter('convertToPath', function() {
	return function(input) {
		var source = input.replace(" & ", "-and-").replace(" ", "-");
		return source;
	}
})

app.filter('index', function() {
	return function(array, index) {
		if (!index)
			index = 'index';
		for (var i = 0; i < array.length; ++i) {
			array[i][index] = i;
		}
		return array;
	};
});

app.filter('titlecase', function() {
	return function(input) {
		if (input) {
			var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

			input = input.toLowerCase();
			return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
				if (index > 0 && index + match.length !== title.length &&
					match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
					(title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
					title.charAt(index - 1).search(/[^\s-]/) < 0) {
					return match.toLowerCase();
				}

				if (match.substr(1).search(/[A-Z]|\../) > -1) {
					return match;
				}

				return match.charAt(0).toUpperCase() + match.substr(1);
			});
		}
	}
});

app.filter('statetitlecase', function() {
	return function(input) {
		if (input.length > 3) {
			var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

			input = input.toLowerCase();
			return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
				if (index > 0 && index + match.length !== title.length &&
					match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
					(title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
					title.charAt(index - 1).search(/[^\s-]/) < 0) {
					return match.toLowerCase();
				}

				if (match.substr(1).search(/[A-Z]|\../) > -1) {
					return match;
				}

				return match.charAt(0).toUpperCase() + match.substr(1);
			});
		} else {
			return input;
		}
	}
});

app.filter('globalFilter', function() {
	return function(items, searchTerm) {
		if (items) {
			var results = []
			if (searchTerm) {
				searchTerm = searchTerm.toLowerCase()
			}
			for (var i in items) {
				var item = makeSortString(items[i].Name.toLowerCase())
				if (item.indexOf(searchTerm) === 0) {
					results.push(items[i])
				} else if (items[i].BaseSku) {
					if (items[i].BaseSku.toLowerCase().indexOf(searchTerm) === 0) {
						results.push(items[i])
					}
				}
			}
			return results
		} else {
			return items
		}
	}
})

function makeSortString(s) {
	if (!makeSortString.translate_re) makeSortString.translate_re = /[éáűőúöüóíÉÁŰPŐÚÖÜÓÍ]/g;
	var translate = {
		"é": "e",
		"á": "a",
		"ű": "u",
		"ő": "o",
		"ú": "u",
		"ö": "o",
		"ü": "u",
		"ó": "o",
		"í": "i",
		"É": "E",
		"Á": "A",
		"Ű": "U",
		"Ő": "O",
		"Ú": "U",
		"Ö": "O",
		"Ü": "U",
		"Ó": "O",
		"Í": "I"
	};
	return (s.replace(makeSortString.translate_re, function(match) {
		return translate[match];
	}));
}

app.filter('advancedFilters', function() {
	return function(dresses, filters) {
		if (!dresses) {
			return []
		}
		var hasFilters = _.find(filters, function(f) {
			return f.length;
		})
		if (filters && hasFilters) {
			return _.filter(dresses, function(d) {
				var filterMatches = _.filter(filters, function(f) {
					return _.intersection(d.Categories, f).length;
				});
				return filterMatches.length == _.filter(filters, function(f) {
					return f.length;
				}).length;
			});
		} else {
			return dresses;
		}

	}
});

app.filter('sortEvents', function() {
	return function(input) {
		var events = [];
		var after = [];
		for (var i in input) {
			if (input[i].EventType == 'First Look') {
				events.push(input[i]);
			} else {
				after.push(input[i]);
			}
		}
		for (var i in after) {
			events.push(after[i]);
		}

		return events;
	}
});

app.filter('truncate', function() {
	return function(text, length, end) {
		if (isNaN(length))
			length = 10;

		if (end === undefined)
			end = "...";

		if (text.length <= length || text.length - end.length <= length) {
			return text;
		} else {
			return String(text).substring(0, length - end.length) + end;
		}

	};
});

app.filter('premierfirst', function() {
	return function(items) {
		if (items) {
			var premier = [];
			var others = [];
			for (var i in items) {
				if (items[i].ProductLines[0].Premier !== 0) {
					premier.push(items[i]);
				}
			}
			premier.sort(function(a, b) {
				return b.ProductLines[0].Premier - a.ProductLines[0].Premier
			})
			for (var i in items) {
				if (items[i].ProductLines[0].Premier == 0) {
					others.push(items[i]);
				}
			}

			for (var i in others) {
				premier.push(others[i])
			}
			return premier;
		} else {
			return items;
		}


	};
});

app.filter('nospace', function() {
	return function(value) {
		return (!value) ? '' : value.replace(/ /g, '-');
	};
});
