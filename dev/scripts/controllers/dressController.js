app.controller('dressController', function($http, favorites, $localStorage, $scope, $routeParams,  dresses, user, maggieBrides, $filter, $window, $rootScope, $sce, $timeout) {
  var dressId = parseInt($routeParams.id);
  $scope.dressId = parseInt($routeParams.id);

  if ($localStorage['dressesOrder']) {
    var dressOrder = JSON.parse($localStorage['dressesOrder']);
    for (var i in dressOrder) {
      if (dressId == dressOrder[i].ProductId) {
        if (i == 0) {
          $scope.previousDress = false;
          $scope.nextDress = dressOrder[parseInt(i) + 1];
        } else if (i == dressOrder.length - 1) {
          $scope.previousDress = dressOrder[parseInt(i) - 1];
          $scope.nextDress = false;
        } else {
          $scope.previousDress = dressOrder[parseInt(i) - 1];
          $scope.nextDress = dressOrder[parseInt(i) + 1];
        }
      }
    }
  } else {
    $scope.previousDress = false;
    $scope.nextDress = false;
  }

  //Add Dress to recently viewed
  dresses.addDressToRecentlyViewed(dressId);

  dresses.getRecentlyViewed().then(function(results) {
    dresses.getDressesById(results).then(function(response) {
      for (var i in response) {
        response[i].Images = $filter('filter')(response[i].Images, 'Medium');

        if (favorites.checkIfFavorited(response[i].ProductId)) {
          response[i].favorited = true;
        } else {
          response[i].favorited = false;
        }
      }

      $scope.recentlyViewed = response;
    })
  })

  //Dress Data
  dresses.getDressById(dressId).then(function(result) {
		console.log(result)
    //categorize dress msrp
    result.MSRP = parseInt(result.MSRP * 1.1);
    if (result.MSRP < 1200) {
      result.MSRP = "$1199 and under";
    } else if (result.MSRP > 1199 && result.MSRP < 1500) {
      result.MSRP = "$1200 - $1499";
    } else if (result.MSRP > 1499 && result.MSRP < 1800) {
      result.MSRP = "$1500 - $1799";
    } else if (result.MSRP > 1799 && result.MSRP < 2200) {
      result.MSRP = "$1800 - $2199";
    } else if (result.MSRP > 2199 && result.MSRP) {
      result.MSRP = "$2200 +"
    }

    //categorize images
    result.imageCategories = {
      High: [],
      HighRes: [],
      Large: [],
      Medium: [],
      Small: [],
      Thumbnail: [],
			Banner: []
    };
    for (var i in result.Images) {
      var resolution = result.Images[i].Tag.split('-').slice(-1);

      if (resolution[0] == 'High') {
        result.imageCategories.High.push(result.Images[i]);
      } else if (resolution[0] == 'HighRes') {
        result.imageCategories.HighRes.push(result.Images[i]);
      } else if (resolution[0] == 'Large') {
        result.imageCategories.Large.push(result.Images[i]);
      } else if (resolution[0] == 'Medium') {
        result.imageCategories.Medium.push(result.Images[i]);
      } else if (resolution[0] == 'Small') {
        result.imageCategories.Small.push(result.Images[i]);
      } else if (resolution[0] == 'Thumbnail') {
        result.imageCategories.Thumbnail.push(result.Images[i]);
      } else if (resolution[0] == 'Banner') {
				result.imageCategories.Banner.push(result.Images[i]);
			}
    }
    result.imageCategories.High = $filter('orderBy')(result.imageCategories.High, 'Sequence')

    $scope.dress = result;

    //Video Banner and Videos
    if (result.VideoPreviewId !== '' && result.VimeoId !== '') {
      $scope.videoSrc = $sce.trustAsResourceUrl('//player.vimeo.com/video/' + result.VideoPreviewId + '?api=1&player_id=player1');
      $scope.videoSrcLong = $sce.trustAsResourceUrl('//player.vimeo.com/video/' + result.VimeoId + '?api=1&player_id=player2');

      //Vimeo Video
      //short video
      var iframe = $('#player1')[0];
      var player = $f(iframe);
      //longer video
      var iframe2 = $('#player2')[0];
      var player2 = $f(iframe2);

      function onFinish(id) {
        //$('.banner-image').removeClass('video-ready');
      }

      if ($window.innerWidth > 1024) {
        player.addEvent('ready', function() {
          $('.banner-image').addClass('video-ready');
          setTimeout(function() {
            $('#player1').addClass('fadein');
          }, 1500)

          $scope.videoReady = true;
          player.api('play');
          player.addEvent('finish', onFinish);
          player.api('setVolume', 0);
          player.api('setLoop', true);
        });

        player2.addEvent('ready', function() {
          player2.addEvent('finish', $scope.stopVideo);
        });
      } else {
        setTimeout(function() {
          $('.banner-image').css('opacity', '1');
        }, 500)
      }

      $scope.playVideo = function() {
        $('.banner-image').addClass('video-ready');
        $('.fullscreen-video').addClass('show');
        player2.api('play');
      }

      $scope.stopVideo = function() {
        $('.fullscreen-video').removeClass('show');
        player2.api('pause');
      }
    } else {
      $scope.playVideo = function() {}
      $scope.stopVideo = function() {}
      setTimeout(function() {
        $('.banner-image').css('opacity', '1');
      }, 500)
    }

    $scope.videoBanner = '//ms-cdn.maggiesottero.com/product/Content/Images/' + result.imageCategories.Banner[0].PictureId + '/' + result.imageCategories.Banner[0].FileName + '';

    //SEO
    $('meta[name=description]').attr('content', result.MetaDescription);
    $('meta[name=title]').attr('content', result.MetaTitle);
    $('title').html(result.Title);
    window.prerenderReady = true;

  });

  //Like Dress
  $scope.likeSelectedDress = function(dressId) {
    favorites.toggleFavoriteDress(dressId).then(function(favorited) {
      if (favorited) {
        $scope.favorited = true;
      } else {
        $scope.favorited = false;
      }
    })

  };

  //Check if favorited
  if (favorites.checkIfFavorited(dressId)) {
    $scope.favorited = true;
  } else {
    $scope.favorited = false;
  }

  //Like related and recently viewed Dresses
  $scope.likeDress = function(dressId, dress) {
    favorites.toggleFavoriteDress(dressId).then(function(favorited) {
      if (favorited) {
        dress.favorited = true;
      } else {
        dress.favorited = false;
      }
    })

  };

  $scope.showImageZoomSlider = function(index) {

    $scope.selectedZoomImageSrc = '//ms-cdn.maggiesottero.com/product/Content/Images/' + $scope.dress.imageCategories.High[index].PictureId + '/' + $scope.dress.imageCategories.High[index].FileName + '';
    $('.image-zoom').css('display', 'block');
    $('.image-zoom').css('opacity', '1');
    $('.image-zoom').css('pointer-events', 'auto');
    $('body').addClass('no-scroll');
  }

  $scope.hideImageZoom = function() {
    $('.image-zoom').css('opacity', '0');
    $('.image-zoom').css('pointer-events', 'none');
    $('body').removeClass('no-scroll');
  }

  $scope.toggleClose = function() {
    $scope.zoomOn = !$scope.zoomOn;
  }

  //Get Related Dresses
  dresses.getRelatedDresses(dressId).then(function(results) {
    $scope.relatedDresses = results.splice(0, 2);

    for (var i in $scope.relatedDresses) {
      $scope.relatedDresses[i].Images = $filter('filter')($scope.relatedDresses[i].Images, 'Medium');

      if (favorites.checkIfFavorited($scope.relatedDresses[i].ProductId)) {
        $scope.relatedDresses[i].favorited = true;
      } else {
        $scope.relatedDresses[i].favorited = false;
      }
    }


  })

  //Get 1st Maggie Bride Post
  maggieBrides.getPostByDress(dressId).then(function(result) {
    $scope.bridePost = result.data.posts[0];
  })

  //Maggie Bride Post Height Adjustments
  setTimeout(function() {
    //Maggie Bride Post Height Adjustments
    var thumbnail = $('.card .thumbnail');
    var width = thumbnail.css('width');
    var height = parseInt(width) * 0.6;
    thumbnail.css('height', '' + height + 'px');
    //Card Height Adjustment
    var firstcardHeight = $('.card.first').outerHeight();
    $('.card.second').css('height', firstcardHeight);
  }, 1000)

  //Card Height Adjustment
  var firstcardHeight = $('.card.first').outerHeight();
  $('.card.second').css('height', firstcardHeight);

  //On window resize
  $(window).resize(function() {
    //Maggie Bride Post Height Adjustments
    var thumbnail = $('.card .thumbnail');
    var width = thumbnail.css('width');
    var height = parseInt(width) * 0.6;
    thumbnail.css('height', '' + height + 'px');
    //Card Height Adjustment
    var firstcardHeight = $('.card.first').outerHeight();
    $('.card.second').css('height', firstcardHeight);
  })

  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);

});
