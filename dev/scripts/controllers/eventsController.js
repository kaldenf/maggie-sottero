app.controller('eventsController', function($scope,  events, cities, $localStorage, googleMaps, $window, user, $filter, $timeout) {

  $scope.eventsLoading = true
  var searchingResults = false
  $scope.radius = 50
  $scope.search

  getEventsNearMe()

  //Populate Prediction List
	cities.getAddresses().then(function(cities){
		$scope.cities = cities
	})

  //Show all results
  $scope.eventsLimit = 5
  $scope.showAllResults = function() {
      $scope.eventsLimit = 500
      $scope.allResultsShown = true
    }
  //Search Call
  $scope.findEvents = function() {
      if ($scope.search) {
        //empty results and show loader
        $scope.eventsLoading = true
        $scope.events = []
        $scope.hidePrediction = true
        searchingResults = true

        googleMaps.getLocation($scope.search).then(function(results) {
          getEvents(results.lng, results.lat)
        })
      }
    }
    //Get Stores

  //Truncate Titles on mobile
  $scope.truncateSize = 100
  if ($window.innerWidth < 620) {
    $scope.truncateSize = 21
  }

  //Group search
  $scope.showAllUSAEvents = function(){
		$scope.eventsLoading = true
		$scope.events = null
    $scope.groups = null
		$scope.search = ''
		events.getInCountry(1).then(function(events){
			stores = $filter('orderBy')(events, 'Name')
			$scope.groups = events
      $scope.countrySearch = true
			$scope.eventsLoading = false
		})
	}

	$scope.showAllInternationalEvents = function(){
    $scope.eventsLoading = true
		$scope.events = null
    $scope.groups = null
		$scope.search = ''
		events.getInternationalEvents().then(function(events){
			events = $filter('orderBy')(events, 'Name')
			$scope.groups = events
      $scope.countrySearch = true
			$scope.eventsLoading = false
		})
	}

  //Get Map
  $scope.hideMap = function(){
    $scope.showMap = false
  }
  function initMap(center, markers) {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: center,
      scrollwheel: false,
      zoom: 8
    });
    var infowindow = new google.maps.InfoWindow();
    for(var i in markers){
      var marker = new google.maps.Marker({
        position:markers[i].position,
        map:map
      })
      google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
          infowindow.setContent(markers[i].title);
          infowindow.open(map, marker);
        }
      })(marker, i));

      google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
        return function() {
          infowindow.close(map, marker);
        }
      })(marker, i));
    }
  }


  //Search UX
  $('#search').keypress(function() {
    $scope.hidePrediction = false
  })
  $(document).mouseup(function(e) {
    var container = $(".prediction-box")

    if (!container.is(e.target) // if the target of the click isn't the container...
      && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $scope.hidePrediction = true
      $scope.$apply()
    }
  })

  //Scope Helper Functions
  $scope.greaterThan = function(prop, val){
    if(prop){
      if(prop.length > val){
        return prop
      }
      else {
        return null
      }
    }
  }

  $scope.adjustMiles = function(radius){
		$scope.radius = radius

		if(!$scope.search){
			getEventsNearMe()
		}
		else{
			$scope.findEvents()
		}
	}

  $scope.expandSearch = function(){
    $scope.radius = $scope.radius*2

		if(!$scope.search){
      $scope.eventsLoading = true
      $scope.events = []
      getEventsNearMe()
		}
		else{
			$scope.findEvents()
		}
  }

  $scope.setSearch = function(city) {
		$scope.search = city.City +', '+ city.State +', '+ city.CountryName +' '+ city.PostalCode
		$scope.hidePrediction = true
		$scope.findEvents()
	}

  $scope.getEventText = function(event) {
    if (event == 'First Looks') {
      return "Try on our newest gowns! During this exclusive event, future brides will have the opportunity to get an up close look at the gowns. Be the first to wear the latest dresses!"
    } else if (event == 'Trunk Shows') {
      return "Trunk Shows offer a large selection of gowns from our current collection, specially brought in by your Authorized Retailer. This one-weekend event is a great opportunity to try on even more wedding dresses!"
    } else if (event == 'Store Events') {
      return "Store events feature Authorized Retailers' favorite styles, creating a memorable experience for the bride. Looking for a fun way to shop with your bridal party? Try a Store Event!"
    } else if (event == 'Unveiled') {
      return "Tried and true, Unveiled events feature our best-selling styles from multiple seasons, perfect for the bride just beginning the search for her dream wedding dress."
    }
  }

  //Helper Functions
  function getLocationByIP(){
		$scope.$apply(function(){
			$scope.events = []
			$scope.eventsLoading = false
			$scope.eventNotFound = true
		})
	}

  function getEventsNearMe(){
    if ("geolocation" in navigator) {
      $window.navigator.geolocation.getCurrentPosition(function(results) {
        if (!searchingResults) {
          googleMaps.getAddress(results.coords.longitude, results.coords.latitude).then(function(location) {
            $localStorage['country'] = location.results[0].address_components[5].short_name
          })
          events.get(results.coords.longitude, results.coords.latitude, $scope.radius).then(function(events) {
            if (!searchingResults) {
              $scope.events = events
              $scope.eventsLoading = false

              var center = {lat: results.coords.latitude, lng: results.coords.longitude}
        			var getMarkers = function(){
        				var array = []
        				for(var i in events){
        					var object = {
        						position:{
        							lat:events[i].Latitude,
        							lng:events[i].Longitude
        						},
        						title:events[i].Name
        					}
        					array.push(object)
        				}
        				return array
        			}
        			initMap(center, getMarkers())
            }
          })
        }
      }, function(error) {
        getLocationByIP()
      })

    } else {
      getLocationByIP()
    }
  }

  function getEvents(lng, lat){
    $scope.groups = null
    events.get(lng, lat, $scope.radius).then(function(events) {
      $scope.events = events
      $scope.eventsLoading = false
      $scope.countrySearch = false
      var center = {lat: lat, lng: lng}
			var getMarkers = function(){
				var array = []
				for(var i in events){
					var object = {
						position:{
							lat:events[i].Latitude,
							lng:events[i].Longitude
						},
						title:events[i].Name
					}
					array.push(object)
				}
				return array
			}
			initMap(center, getMarkers())

    })
  }

  //Mobile Settings
  $scope.truncateSize = 100
  if ($window.innerWidth < 620) {
    $scope.truncateSize = 21
  }

  //Page Resizing
  var windowHeight = $(window).height()
  $('.page').css('min-height', '' + windowHeight - 355 + 'px')

  $('title').html('Find a Bridal Event Near You | Maggie Sottero')
  $('meta[name=title]').attr('content', 'Find a Bridal Event Near You | Maggie Sottero')
  $('meta[name=description]').attr('content', '')
  window.prerenderReady = true

  $timeout(function() {
    window.scrollTo(0, 0)
  }, 0)

})
