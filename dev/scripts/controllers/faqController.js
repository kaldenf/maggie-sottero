app.controller('faqController', function($scope, $route, $routeParams, $sce, $timeout) {

    var windowHeight = $(window).height();
	$('.page').css('min-height', ''+ windowHeight-355 + 'px');

	//handle links to tabs
	if($routeParams.tab){
		$scope.tab = $routeParams.tab;
	}
	else {
		$scope.tab = 'gown';
	}

	$('title').html('FAQ | Maggie Sottero Wedding Dresses');
	$('meta[name=title]').attr('content', 'FAQ | Maggie Sottero Wedding Dresses');
    $('meta[name=description]').attr('content','');
    window.prerenderReady = true;

    $timeout(function(){
		window.scrollTo(0,0);
	},0);

});
