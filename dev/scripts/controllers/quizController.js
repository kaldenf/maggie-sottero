app.controller('quizController', function($scope, $localStorage, $window,  dresses, $rootScope, user, $timeout, googleMaps, favorites) {

  $rootScope.hideFooter = true;

  var token = $localStorage['jwtToken'];
  var userId = $localStorage['userId'];

  var questions = [{
    "question": "What is your must-have wedding day accessory?",
    "answers": [{
      "answer": "Heirloom jewelry",
      "image": "2_heirloomjewelry.png",
      "category": 1
    }, {
      "answer": "Avante-garde headpiece",
      "image": "2_avantgardeheadpiece.png",
      "category": 2
    }, {
      "answer": "Floral Crown",
      "image": "2_floralcrown.png",
      "category": 3
    }, {
      "answer": "Bold lip",
      "image": "2_boldlip.png",
      "category": 4
    }, {
      "answer": "Diamond earrings",
      "image": "2_diamondearrings.png",
      "category": 5
    }, {
      "answer": "Flip Flops",
      "image": "2_flipflops.png",
      "category": 6
    }]
  }, {
    "question": "What is your wedding makeup style?",
    "answers": [{
      "answer": "Rosy cheeks and lips",
      "image": "4_rosycheeksandlips.png",
      "category": 1
    }, {
      "answer": "Dramatic cat eye",
      "image": "4_dramaticcateye.png",
      "category": 2
    }, {
      "answer": "Natural, barely there makeup",
      "image": "4_natural.png",
      "category": 3
    }, {
      "answer": "Smoky eyes and contoured cheeks",
      "image": "4_smokyeyes.png",
      "category": 4
    }, {
      "answer": "Tinted lips and shimmering eyes",
      "image": "4_tintedlips.png",
      "category": 5
    }, {
      "answer": "Sun-kissed cheeks and coral lips",
      "image": "4_sunkissedcheeks.png",
      "category": 6
    }]
  }, {
    "question": "What hairstyle do you envision on your wedding day?",
    "answers": [{
      "answer": "Unkempt updo",
      "image": "5_unkemptupdo.png",
      "category": 1
    }, {
      "answer": "Elegant updo with a crystal accessory",
      "image": "5_elegantupdo.png",
      "category": 2
    }, {
      "answer": "Chunky french braid",
      "image": "5_chunkyfrenchbraid.png",
      "category": 3
    }, {
      "answer": "Styled curls and a deep sidepart",
      "image": "5_styledcurls.png",
      "category": 4
    }, {
      "answer": "Chic chignon",
      "image": "5_chicchignon.png",
      "category": 5
    }, {
      "answer": "Relaxed waves",
      "image": "5_relaxedwaves.png",
      "category": 6
    }]
  }, {
    "question": "Where do you envision celebrating your love?",
    "answers": [{
      "answer": "Historic exposed brick loft",
      "image": "7_historicexposedbrickloft.png",
      "category": 1
    }, {
      "answer": "Atrium",
      "image": "7_atrium.png",
      "category": 2
    }, {
      "answer": "Lovely garden",
      "image": "7_lovelygarden.png",
      "category": 3
    }, {
      "answer": "Elegant ballroom",
      "image": "7_elegantballroom.png",
      "category": 4
    }, {
      "answer": "Traditional chapel",
      "image": "7_traditionalchapel.png",
      "category": 5
    }, {
      "answer": "Sandy shores of a beach",
      "image": "7_sandyshoresofabeach.png",
      "category": 6
    }]
  }, {
    "question": "If you could go anywhere in the world for your honeymoon, where would it be?",
    "answers": [{
      "answer": "Wales, UK",
      "image": "8_walesuk.png",
      "category": 1
    }, {
      "answer": "South Africa",
      "image": "8_southafrica.png",
      "category": 2
    }, {
      "answer": "Santorini, Greece",
      "image": "8_santorinigreece.png",
      "category": 3
    }, {
      "answer": "Paris, France",
      "image": "8_parisfrance.jpg",
      "category": 4
    }, {
      "answer": "Venice, Italy",
      "image": "8_veniceitaly.png",
      "category": 5
    }, {
      "answer": "Maui, Hawaii",
      "image": "8_mauihawaii.png",
      "category": 6
    }]
  }, {
    "question": "Which bridal trend do you love the most?",
    "answers": [{
      "answer": "Romantic lace",
      "image": "9_romanticlace.png",
      "category": 1
    }, {
      "answer": "A show-stopping back",
      "image": "9_showstoppingback.png",
      "category": 2
    }, {
      "answer": "Ethereal flowing skirt",
      "image": "9_etherealflowingskirt.png",
      "category": 3
    }, {
      "answer": "Glittering swarovski crystal",
      "image": "9_glitteringswarovskicrystal.png",
      "category": 4
    }, {
      "answer": "Fairytale ball gown",
      "image": "9_fairytaleballgown.png",
      "category": 5
    }, {
      "answer": "Soft light-weight fabric",
      "image": "9_softlightweightfabric.png",
      "category": 6
    }]
  }, {
    "question": "What does your dream date night look like?",
    "answers": [{
      "answer": "Dinner for two",
      "image": "10_dinnerfortwo.png",
      "category": 1
    }, {
      "answer": "Exhibit at a museum",
      "image": "10_exhibitatamuseum.png",
      "category": 2
    }, {
      "answer": "Outdoor concert",
      "image": "10_outdoorconcert.png",
      "category": 3
    }, {
      "answer": "Wine and cheese tasting",
      "image": "10_wineandcheesetasting.png",
      "category": 4
    }, {
      "answer": "Dinner and a movie",
      "image": "10_dinnerandamovie.png",
      "category": 5
    }, {
      "answer": "Street fair",
      "image": "10_streetfair.png",
      "category": 6
    }]
  }, {
    "question": "Which color palette most closely resembles your wedding colors?",
    "answers": [{
      "answer": "Dusty pastels",
      "image": "11_dustypastels.png",
      "category": 1
    }, {
      "answer": "Monochromatic shades of white with a pop of color",
      "image": "11_monochromaticshades.png",
      "category": 2
    }, {
      "answer": "Lots of greenery, accented by lush blooms",
      "image": "11_lotsofgreenery.png",
      "category": 3
    }, {
      "answer": "Black, white, glittering gold and a pop of color",
      "image": "11_blackwhiteglitteringgold.png",
      "category": 4
    }, {
      "answer": "Navy, white, and a touch of emerald green",
      "image": "11_navywhiteemeraldgreen.png",
      "category": 5
    }, {
      "answer": "Rich, jewel tones",
      "image": "11_richjeweltones.png",
      "category": 6
    }]
  }, {
    "question": "Let's talk food... Which types of food will you have at your wedding?",
    "answers": [{
      "answer": "Sweet treats on vintage platters",
      "image": "12_sweettreatsonvintageplatters.png",
      "category": 1
    }, {
      "answer": "Food truck",
      "image": "12_foodtruck.png",
      "category": 2
    }, {
      "answer": "Charcuterie plate",
      "image": "12_charcuterieplate.png",
      "category": 3
    }, {
      "answer": "5-course meal",
      "image": "12_fivecoursemeal.png",
      "category": 4
    }, {
      "answer": "Our favorite foods and wedding cake",
      "image": "12_ourfavoritefoodandweddingcake.png",
      "category": 5
    }, {
      "answer": "Local cuisine",
      "image": "12_localcuisine.png",
      "category": 6
    }]
  }, {
    "question": "It's time to decide your wedding party... How many members are in your wedding party?",
    "answers": [{
      "answer": "4",
      "image": "13_4.png",
      "category": 1
    }, {
      "answer": "6",
      "image": "13_6.png",
      "category": 2
    }, {
      "answer": "8",
      "image": "13_8.png",
      "category": 3
    }, {
      "answer": "10",
      "image": "13_10.png",
      "category": 4
    }, {
      "answer": "12",
      "image": "13_12.png",
      "category": 5
    }, {
      "answer": "We aren't having a bridal party, but our closest friends will be involved.",
      "image": "13_noparty.png",
      "category": 6
    }]
  }, {
    "question": "Where is your favorite place to shop?",
    "answers": [{
      "answer": "Etsy",
      "image": "14_etsy.png",
      "category": 1
    }, {
      "answer": "Miu Miu",
      "image": "14_miumiu.png",
      "category": 2
    }, {
      "answer": "Temperley London",
      "image": "14_temperleylondon.png",
      "category": 3
    }, {
      "answer": "Zara",
      "image": "14_zara.png",
      "category": 4
    }, {
      "answer": "Gap",
      "image": "14_gap.png",
      "category": 5
    }, {
      "answer": "Local boutique",
      "image": "14_localboutique.png",
      "category": 6
    }]
  }, {
    "question": "Which bouquet do you envision for your wedding day?",
    "answers": [{
      "answer": "Mixed wildflowers",
      "image": "15_mixedwildflowers.png",
      "category": 1
    }, {
      "answer": "Dramatic and deep hues",
      "image": "15_dramaticanddeephues.png",
      "category": 2
    }, {
      "answer": "Unique seasonal flowers and succulents",
      "image": "15_uniqueseasonalflowers.png",
      "category": 3
    }, {
      "answer": "Pale pink bouquet ",
      "image": "15_palepinkbouquet.png",
      "category": 4
    }, {
      "answer": "Sleek bouquet of roses",
      "image": "15_sleekbouquetofroses.png",
      "category": 5
    }, {
      "answer": "Colorful flowers",
      "image": "15_colorfulflowers.png",
      "category": 6
    }]
  }, {
    "question": "What is the most important aspect of your wedding?",
    "answers": [{
      "answer": "DIY details",
      "image": "16_diydetails.png",
      "category": 1
    }, {
      "answer": "The photographer",
      "image": "16_thephotographer.png",
      "category": 2
    }, {
      "answer": "Flowers",
      "image": "16_flowers.png",
      "category": 3
    }, {
      "answer": "The attire",
      "image": "16_theattire.png",
      "category": 4
    }, {
      "answer": "Centerpieces",
      "image": "16_centerpieces.png",
      "category": 5
    }, {
      "answer": "Ceremony",
      "image": "16_ceremony.png",
      "category": 6
    }]
  }, {
    "question": "How many guests are you expecting at your wedding?",
    "answers": [{
      "answer": "100-150",
      "image": "17_100-150.png",
      "category": 1
    }, {
      "answer": "150-200",
      "image": "17_150-200.png",
      "category": 2
    }, {
      "answer": "50-100",
      "image": "17_50-100.png",
      "category": 3
    }, {
      "answer": "250 +",
      "image": "17_250plus.png",
      "category": 4
    }, {
      "answer": "200-250",
      "image": "17_200-250.png",
      "category": 5
    }, {
      "answer": "Anyone that is willing to make the journey",
      "image": "17_anyonethatiswilling.png",
      "category": 6
    }]
  }, {
    "question": "What will your groom be wearing on your wedding day?",
    "answers": [{
      "answer": "A woven vest and tie",
      "image": "18_wovenvestandtie.png",
      "category": 1
    }, {
      "answer": "Suspenders and a bow-tie",
      "image": "18_suspendersandabowtie.png",
      "category": 2
    }, {
      "answer": "A gingham patterned shirt and pocket square",
      "image": "18_ginghampatternandpocketsquare.png",
      "category": 3
    }, {
      "answer": "A classic tuxedo",
      "image": "18_classictuxedo.png",
      "category": 4
    }, {
      "answer": "A timeless suit and matching tie",
      "image": "18_timelesssuitandmatchingtie.png",
      "category": 5
    }, {
      "answer": "Casual is key",
      "image": "18_casualiskey.png",
      "category": 6
    }]
  }, {
    "question": "What type of music will you have playing at your wedding?",
    "answers": [{
      "answer": "40s vocalists",
      "image": "19_fortiesvocalists.png",
      "category": 1
    }, {
      "answer": "Rock and roll band",
      "image": "19_rockandrollband.png",
      "category": 2
    }, {
      "answer": "Bongo drums",
      "image": "19_bongodrums.png",
      "category": 3
    }, {
      "answer": "String quartet",
      "image": "19_stringquartet.png",
      "category": 4
    }, {
      "answer": "DJ",
      "image": "19_dj.png",
      "category": 5
    }, {
      "answer": "Mariachi band",
      "image": "19_mariachiband.png",
      "category": 6
    }]
  }, {
    "question": "It's time for the grand exit... What's your getaway car?",
    "answers": [{
      "answer": "Vintage car",
      "image": "20_vintagecar.png",
      "category": 1
    }, {
      "answer": "Bright red speedster",
      "image": "20_brightredspeedster.png",
      "category": 2
    }, {
      "answer": "Bicycles",
      "image": "20_bicycles.png",
      "category": 3
    }, {
      "answer": "Glamorous car",
      "image": "20_glamorouscar.png",
      "category": 4
    }, {
      "answer": "Classic black car",
      "image": "20_classicblackcar.png",
      "category": 5
    }, {
      "answer": "Personal boat",
      "image": "20_personalboat.png",
      "category": 6
    }]
  }];

  //Quiz Functionality
  $scope.category = [];
  $scope.answers = [];

  //sets yourStyle to Text, gets Description, and filterId
  var getStyleDresses = function(myStyle) {
    //Get Filter Id and your style name
    var filterId;
    switch (parseInt(myStyle)) {
      case 1:
        $scope.yourStyle = 'Vintage';
        $scope.yourStyleDescription = 'Inspired by iconic elements of glamorous Old Hollywood, these vintage style wedding dresses call to romantic brides seeking a vintage twist. These retro-inspired beauties glimmer with geometric metallic embellishments, subtle sequins and opalescent pearls. Drawing inspiration from quintessential elements of the 1920s, these vintage wedding dresses encompass Neo-classic columns, Art Deco designs, and jewelry inspired beading. Vintage inspired capelets offer demure coverage with a vintage flair. Read our favorite vintage inspired wedding trends below.';
        $scope.filterId = 498;
        $scope.categoryId = 488;
        break;
      case 2:
        $scope.yourStyle = 'Statement';
        $scope.yourStyleDescription = 'For the bride seeking a statement wedding dress, look no further than these show-stopping beauties. Complete with intricate back detailing, glamorous skirts, and decadent Swarovski crystal beading, statement wedding dresses are absolutely show-stopping. Pair a statement wedding dress with an elegant updo, accented with an avant-garde hairpiece, and a dramatic cat-eye, mirroring the sophistication of your wedding day.';
        $scope.filterId = 497;
        $scope.categoryId = 487;
        break;
      case 3:
        $scope.yourStyle = 'Bohemian';
        $scope.yourStyleDescription = 'From breezy poet sleeves to airy layers of chiffon, bohemian wedding dresses are perfect for the free-spirited bride. These ethereal, whimsical looks are accented by lightweight, barely-there fabrics and streamlined silhouettes. The bohemian bride is laidback and carefree, accenting her wedding with rustic touches… A lush floral crown, natural makeup, and a chunky braid complete your wedding day look.';
        $scope.filterId = 493;
        $scope.categoryId = 483;
        break;
      case 4:
        $scope.yourStyle = 'Glamour';
        $scope.yourStyleDescription = 'From sparkling Swarovski crystal embellishments to breathtaking skirts, dramatic, detailed backs to plunging necklines, glamorous wedding dresses are sure to leave your guests talking about your gown long after the big day. Read our favorite elements of a glamorous wedding dress below. Channel your inner starlet when styling your glamorous wedding dress, choosing contoured cheeks, smoky eyes, and loose curls, parted dramatically to the side.';
        $scope.filterId = 496;
        $scope.categoryId = 486;
        break;
      case 5:
        $scope.yourStyle = 'Classic';
        $scope.yourStyleDescription = "While we're all for modern and fashion-forward weddings, there is something to be said for a celebration that will never go out of style. Aiming for that timeless look? Traditional and elegant, classic wedding dresses stand the test of time. Incorporating traditional elements with romantic details, classic wedding dresses hold a special place in our hearts. See some of our favorites, and read more about achieving your timeless wedding day look below. A sleek bouquet of roses, a chic chignon, and elegant pair of diamond drop earring complement your classic wedding day.";
        $scope.filterId = 494;
        $scope.categoryId = 484;
        break;
      case 6:
        $scope.yourStyle = 'Beach';
        $scope.yourStyleDescription = 'With stunning beaches, endless expanses of white sand, picturesque sunsets, and turquoise water, beach weddings always make us weak in the knees. A sandy beach offers a perfect backdrop for a destination wedding. When planning a beach wedding, choose lightweight fabrics and fuss-free dresses, allowing you to focus on your soon to-be-husband and the sand between your toes. Pair your beach wedding with tousled, relaxed waves, sun-kissed bronzed cheeks, and a subtle coral lip.';
        $scope.filterId = 495;
        $scope.categoryId = 485;
        break;
      default:
        $scope.yourStyle = '';
        $scope.yourStyleDescription = "none";
        break;
    }
    //Get Dresses
    dresses.getDressesByCategory(2, $scope.filterId).then(function(results) {
      $scope.maggieDresses = results;
    });

    dresses.getDressesByCategory(3, $scope.filterId).then(function(results) {
      $scope.midgleyDresses = results;
    })

  };
  //saves user style
  var saveUserStyle = function(styleId) {
    if (token || userId) {
      if ($localStorage['myStyle']) {
        styleId = $localStorage['myStyle'];
        delete $localStorage['myStyle'];
      }
      user.getInfo().then(function(response) {
        var user = {
          email: response.email,
          your_style: styleId
        }
        user.update(user).then(function(response) {

        })
      });
    } else {
      $localStorage['myStyle'] = styleId;
    }
  };

  //Determine style Id from answered questions
  var getMyStyle = function(answers) {
    var sorted_array = answers.sort();
    for (var i in sorted_array) {
      if ($.isArray(sorted_array[i])) {
        for (var x in sorted_array[i]) {
          sorted_array.push(sorted_array[i][x]);
        }
        sorted_array.splice(i, 1);
      }
    }
    var highest = [];
    var dups = 1;
    for (var i = 0; i <= answers.length - 1; i++) {

      if (sorted_array[i + 1] == sorted_array[i]) {
        dups++;
      } else {
        var cat = {
          'category': sorted_array[i],
          'count': dups
        };

        if (highest.length > 0) {
          for (var x = 0; x < highest.length; x++) {
            if (dups > highest[x].count) {
              highest = [];
              highest[0] = cat;
              dups = 1;
              break;
            } else if (dups == highest[x].count) {
              highest.push(cat);
              dups = 1;
              break;
            }
          }
        } else {
          highest[0] = cat;
          dups = 1;
        }
      }
    }
    if (highest.length > 1) {
      var order = [5, 1, 4, 3, 2, 6];
      var final_category;
      for (var i = 0; i < highest.length; i++) {
        if (final_category == undefined) {
          final_category = highest[i].category;
        } else {
          $.each(order, function(index, value) {
            if (value == highest[i].category) {
              if (index < final_category) {
                final_category = index;
              }
              return false;
            }
          });
        }
      }
      return (order[final_category]);
    } else {
      return (highest[0].category);
    }
  };

  //Shuffle questions
  var shuffle = function(o) {
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
  };
  for (var i = 0; i < questions.length; i++) {
    questions[i].answers = shuffle(questions[i].answers);
  };
  //Get random 10, but always use first one
  var randomizeQuestions = function(sourceArray, neededElements) {
    var result = [];
    for (var i = 0; result.length < neededElements; i++) {
      var question = sourceArray[Math.floor(Math.random() * sourceArray.length)];
      if ($.inArray(question, result) == -1) {
        result.push(question);
      } else {
        i--;
      }
    }
    return result;
  };
  //Set questions
  $scope.questions = randomizeQuestions(questions, 10, questions[0]);

  //If already has a style
  $scope.loading = true;
  if (token) {
    user.getInfo().then(
      function(response) {
        if (response.data.user.your_style !== 0 && response.data.user.your_style !== undefined) {
          $scope.quizComplete = true;
          $scope.yourStyleId = response.data.user.your_style;
          $scope.yourStyle = response.data.user.your_style;
          getStyleDresses(response.data.user.your_style);
        } else {
          $scope.startScreen = true;
        }
        $scope.loading = false;
      }
    );
  } else {
    if ($localStorage['myStyle']) {
      $scope.quizComplete = true;
      $scope.yourStyle = $localStorage['myStyle'];
      $scope.yourStyleId = $localStorage['myStyle'];
      getStyleDresses($scope.yourStyle);
    } else {
      $scope.startScreen = true;
    }
    $scope.loading = false;
  }

  //Questionair slider functionalities
  $scope.current = 0;
  $scope.nextQuestion = function() {
    if ($scope.current < $scope.questions.length - 1) {
      $scope.current++;
    }
  };
  $scope.previousSlide = function() {
    if ($scope.current > 0) {
      $scope.current--;
    }
  };
  $scope.nextSlide = function(index) {
    if ($scope.answers[index] !== undefined) {
      window.scrollTo(0, 0);
      if ($scope.current < $scope.questions.length - 1) {
        $scope.current++;
      } else {
        //Last Slide
        //Show intermission
        $('.intermission').addClass('show');
        $('body').addClass('no-scroll');
        //empty any past dresses
        $timeout(function() {
          $scope.quizComplete = true;
        }, 400);

        $scope.maggieDresses = false;
        $scope.midgleyDresses = false;
        //Get style id from answers
        var myStyle = getMyStyle($scope.category);
        //Get Dresses for style id
        getStyleDresses(myStyle);

        //Save your style
        saveUserStyle(myStyle);
        $timeout(function() {
          $('.intermission').removeClass('show');
          $('body').removeClass('no-scroll');
        }, 2500)

      }
    }
  };

  //Retake Quiz
  $scope.retakeQuiz = function() {
    $scope.quizComplete = false;
    $scope.yourStyle = '';
    $scope.answers = [];
    $scope.current = 0;
    $scope.questions = randomizeQuestions(questions, 10, questions[0]);
  }

  if ($window.innerWidth < 620) {
    $scope.slidesToShow = 1;
  } else if ($window.innerWidth > 620 && $window.innerWidth < 1024) {
    $scope.slidesToShow = 3;
  } else {
    $scope.slidesToShow = 4;
  }

  //Get Maggie Picks
  var getMaggiePicks = function() {
    dresses.getMaggiePicks().then(function(results) {
      $scope.maggiePicks = results;
    })
  }
  if ($localStorage['country']) {
    getMaggiePicks();
  } else {
    if ("geolocation" in navigator) {
      $window.navigator.geolocation.getCurrentPosition(function(results) {
        googleMaps.getAddress(results.coords.longitude, results.coords.latitude).then(function(location) {
          $localStorage['country'] = location.results[0].address_components[5].short_name;
          getMaggiePicks();

        })
      }, function(error) {
        user.getLocationByIP().then(function(response) {
          $localStorage['country'] = response.country_code2;
          getMaggiePicks();
        })
      });
    } else {
      user.getLocationByIP().then(function(response) {
        $localStorage['country'] = response.country_code2;
        getMaggiePicks();
      })
    }
  };

  //Like Dress
  $scope.likeDress = function(dressId, dress) {
    favorites.toggleFavoriteDress(dressId).then(function(favorited) {
      if (favorited) {
        dress.favorited = true;
      } else {
        dress.favorited = false;
      }
    })
  };

  //Page Resizer
  var windowHeight = $(window).height();
  $('.page').css('min-height', '' + windowHeight - 355 + 'px');

  $scope.quizfacebookShare = function() {
    FB.ui({
      method: 'feed',
      link: 'https://maggiesottero.com/quiz',
      caption: 'Maggie Sottero',
      description: 'I took Maggie Sottero’s ‘Find Your Fit’ Quiz! I am a %23' + $scope.yourStyle + 'bride! Take the quiz to find your wedding day style!',
      picture: 'https://maggiesottero.com/images/' + $scope.yourStyle + '-Bride.jpg'
    }, function(response) {});
  }

  setTimeout(function() {
    window.scrollTo(0, 0);
  }, 50);

  $('meta[name=description]').attr('content', "For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");
  $('meta[name=title]').attr('content', 'What Kind of Bride Are You? Take the Quiz and Find Out');
  document.title = 'What Kind of Bride Are You? Take the Quiz and Find Out';

});
