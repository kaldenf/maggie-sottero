app.controller('loginController', function($scope, $localStorage, $rootScope, $location, user, $window, facebook, $timeout) {

  $rootScope.hideFooter = true;

  var token = $localStorage['jwtToken'];
  var userId = $localStorage['userId'];

  if (token || userId) {
    $location.path('/home');
  }

  $scope.login = function() {
    user.login($scope.username, $scope.password).then(function(response) {
      $rootScope.loggedIn = true;
      $rootScope.userEmail = $scope.username;
      $rootScope.$apply;
      if ($rootScope.shareWeddingOnLogin) {
        $location.path('/share-wedding');
        $rootScope.shareWeddingOnLogin = false;
      } else {
        $location.path('/account');
      }
    });
  }

  $scope.facebookLogin = function() {
    facebook.login().then(function(response) {
      user.login(response.email, response.id).then(function(response) {
        $rootScope.loggedIn = true;
        $rootScope.userEmail = response.email;
        $rootScope.$apply;
        $location.path("/account");
      });
    });
  }

  $scope.forgotPassword = function() {
    user.forgotPassword($scope.forgotPasswordEmail).then(function(response) {})
  }

  $('meta[name=description]').attr('content', "For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");
  $('meta[name=title]').attr('content', 'Log In | Maggie Sottero');
  document.title = 'Log In to Your Account | Maggie Sottero';
  window.prerenderReady = true;

  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);

});
