app.controller('maggieBrideController', function($scope, $routeParams, user, maggieBrides, dresses,  $sce, $filter, $timeout) {

  var postId = $routeParams.id;
  maggieBrides.getPost(postId).then(function(response) {
    $scope.post = response.data.post;
    $scope.description = $scope.post.about_wedding;
    $scope.post.about_wedding = $sce.trustAsHtml($scope.post.about_wedding);

    $scope.dress = {};
    if (response.data.post.dress_details) {
      dresses.getDressById(response.data.post.dress_details.ProductId).then(function(response) {
        response.Images = $filter('filter')(response.Images, 'Medium');

        if (response.ProductLine == 'Maggie Sottero') {
          $scope.urlDressLine = 'maggie-sottero';
        } else if (response.ProductLine == 'Sottero & Midgley') {
          $scope.urlDressLine = 'sottero-and-midgley';
        }
        $scope.dress = response;
        document.title = 'Maggie Bride: ' + $scope.post.name + ' in ' + $scope.post.dress_details.Name + ' | ' + response.ProductLine + '';
        window.prerenderReady = true;

        $scope.facebookBrideShare = function(){
          FB.ui({
            method: 'feed',
            link: window.location.href,
            caption: $scope.post.dress_details.Name + ' - ' + response.ProductLine,
            description: $scope.description,
            picture: $scope.post.images[0].url
          }, function(response){});
        }

      })
    } else {
      $scope.dress.ProductLine = 'Maggie Sottero';
      document.title = 'Maggie Bride: ' + response.name + ' | Maggie Sottero';
      window.prerenderReady = true;

      $scope.facebookBrideShare = function(){
        FB.ui({
          method: 'feed',
          link: window.location.href,
          caption: 'Maggie Sottero | Maggie Bride',
          description: $scope.description,
          picture: $scope.post.images[0].url
        }, function(response){});
      }

    }

    $('meta[name=description]').attr('content', "For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");

  })

  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);

});
