app.controller('shareWeddingController', function($scope, $location, $rootScope, $window, user, maggieBrides, aws, facebook, dresses, maggieBrides, $timeout) {

  if ($rootScope.userEmail) {
    $scope.email = $rootScope.userEmail;
  }

  //Get token for submission
  maggieBrides.getToken().then(function(maggieToken) {

  })

  $scope.chosenDressName = "Choose";
  $scope.images = [];
  $scope.selectDress = function(dress) {
    $scope.selectedDressName = dress.Name;
    $scope.selectedDress = dress;
  };
  $scope.chooseDress = function() {
    $scope.chosenDress = $scope.selectedDress;
    $scope.chosenDressName = $scope.selectedDressName;
  };

  $scope.loading = true;
  $scope.dressLine = 'maggie-sottero';
  $scope.getDressLine = function(lineId) {
    dresses.getDressesForDressline(lineId).then(function(results) {
      $scope.dresses = results;
      $scope.loading = false;
    })
  }

  $scope.getDressLine(2);

  $scope.submitPost = function() {

    if ($scope.email && $scope.name && $scope.chosenDress && $scope.about_wedding && $scope.images.length > 0) {
      $scope.about_wedding = $scope.about_wedding.replace(/\r?\n/g, '<br />');
      maggieBrides.submitPost($scope.email, $scope.name, $scope.chosenDress, $scope.about_wedding, $scope.images).then(function(response) {
        $scope.email = '';
        $scope.name = '';
        $scope.about_wedding = '';
        $('textarea').css('height', '175px');
        $scope.selectedDressName = 'Choose';
        $scope.selectedDress = '';
        $scope.chosenDressName = 'Choose';
        $scope.images = [];

      })

      $('#completeModal').modal('show');
    } else if (!$scope.name) {
      $rootScope.$broadcast('notification', 'Please enter your name');
    } else if (!$scope.email) {
      $rootScope.$broadcast('notification', 'Please enter a valid email');
    } else if (!$scope.chosenDress) {
      $rootScope.$broadcast('notification', 'Please choose a dress');
    } else if (!$scope.about_wedding) {
      $rootScope.$broadcast('notification', 'Please tell us something about your wedding');
    } else if ($scope.images == 0) {
      $rootScope.$broadcast('notification', 'Please choose at least one photo and add a caption');
    }

  }

  $scope.getFbAlbums = function() {
    facebook.getAlbums().then(function(response) {
      $scope.albums = response;
    })
  }

  $scope.getAlbumPhotos = function(id, name) {
    $scope.selectedAlbumName = name;
    facebook.getAlbum(id).then(function(response) {
      $scope.selectedAlbum = response;
      $scope.showAlbum = true;
    })
  }

  $scope.facebookPhotos = [];
  $scope.facebookSelectedPhotos = [];
  $scope.selectPhoto = function(photo, index) {
    if (!$scope.photoSelected(index)) {
      $scope.facebookPhotos.push(index);
      $scope.facebookSelectedPhotos.push(photo);
    } else {
      $scope.facebookPhotos = jQuery.grep($scope.facebookPhotos, function(value) {
        return value != index;
      });
      $scope.facebookSelectedPhotos = jQuery.grep($scope.facebookSelectedPhotos, function(value) {
        return value != photo;
      });
    }

  }
  $scope.photoSelected = function(index) {
    return $.inArray(index, $scope.facebookPhotos) > -1;
  }

  $scope.upload = function() {
    if ($scope.files) {
      aws.uploadImage($scope.files)
        .then(function(imagePaths) {
          _.each(imagePaths, function(imagePath) {
            $scope.images.push({
              url: imagePath,
              caption: ""
            })
          })
        })
    }
    if ($scope.facebookSelectedPhotos) {
      for (var i in $scope.facebookSelectedPhotos) {
        $scope.images.push({
          url: $scope.facebookSelectedPhotos[i].source,
          caption: ""
        })

      }
    }
    $('#imageModal').modal('hide');

  };

  $scope.clickChooseFile = function() {
    $timeout(function(){
      $('#chooseFile').trigger("click")
    })
  }

  $scope.removeImage = function(index) {
    $scope.images.splice(index, 1);
  }

  $scope.closeModal = function(){
    $('#completeModal').modal('hide')
    window.location.href = '/brides'
  }

  $('#dressModal').on('click', 'li', function() {
    $('#dressModal li').removeClass('active');
    $(this).addClass('active');
  });

  $('textarea').autogrow();
  window.prerenderReady = true;

  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);

});
