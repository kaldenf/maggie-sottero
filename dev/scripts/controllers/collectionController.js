// (function() {
//   'use strict'
//   angular.module('app').directive('collections', collectionsController)
//
//   function collectionsController($scope, favorites, $sce, $http,  dresses, $localStorage, $window, $rootScope, user, $routeParams, $location, $timeout, googleMaps, $filter) {
//     return {
//
//       templateUrl: 'pages/collections.html',
//       controllerAs: 'vm',
//       controller: function() {
//         var vm = this
//
//         angular.extend(vm, {
//           likeDress: likeDress,
//           seasons:null,
//           dressesLoading:true,
//           restLoading:true,
//           searchable:false,
//           filtersLimit:5,
//           filtersActive:[],
//           allSeason:[]
//         })
//
//         init()
//
//         function init(){
//
//         }
//
//         function likeDress(dressId, dress){
//           favorites.toggleFavoriteDress(dressId).then(function(favorited) {
//             if (favorited) {
//               dress.favorited = true;
//             } else {
//               dress.favorited = false;
//             }
//           })
//         }
//
//         function getDresses(){
//
//         }
//
//         function getSeasons(dressLineId){
//
//         }
//
//
//
//         //******** end *********//
//       }
//     }
//   }
//
// })();



app.controller('collectionsController', function($scope, favorites, $sce, $http,  dresses, $localStorage, $window, $rootScope, user, $routeParams, $location, $timeout, googleMaps, $filter) {

  //Like Dress
  $scope.likeDress = function(dressId, dress) {
    favorites.toggleFavoriteDress(dressId).then(function(favorited) {
      if (favorited) {
        dress.favorited = true;
      } else {
        dress.favorited = false;
      }
    })
  };

  //Variables
  $scope.urlDressLine = $location.path().split('/');
  $scope.seasons = [];
  $scope.dressesLoading = true;
  $scope.restLoading = true;
  $scope.searchable = false;
  $scope.filtersLimit = 5;
  $scope.filtersActive = [];

  //Get Dress and Filters Functions
  var getDresses = function(dressLineId) {

    dresses.getSeasons(dressLineId).then(function(results) {
      $scope.allSeasons = results;
      $scope.allDresses = [];

      //Show first season banners
      $scope.seasons = [];
      var firstSeason = results[0];
      for (var i in firstSeason.Images) {
        if (firstSeason.Images[i].Tag == "Collection-Banner") {
          firstSeason.collectionBanner = firstSeason.Images[i];
        } else if (firstSeason.Images[i].Tag == "Collection-Logo-Full") {
          firstSeason.collectionBannerLogo = firstSeason.Images[i];
        }
      }
      firstSeason.showFilmButton = true;
      $scope.seasons.push(firstSeason);

      //Get rest of dresses
      $timeout(function() {
        dresses.getDressesForSeasons($scope.seasons, dressLineId).then(function(results) {
          $scope.dressesLoading = false;
          $scope.seasons = results;
          rememberDressOrder();

          if(!$localStorage['sotteroOpenSeasons']) {
            $localStorage['sotteroOpenSeasons'] = null
          }

          if(!$localStorage['midgleyOpenSeasons']) {
            $localStorage['midgleyOpenSeasons'] = null
          }

          var position
          if($scope.urlDressLine[1] == 'sottero-and-midgley') {
            position = JSON.parse($localStorage['sotteroOpenSeasons'])
          }
          else if($scope.urlDressLine[1] == 'maggie-sottero') {
            position = JSON.parse($localStorage['midgleyOpenSeasons'])
          }

          for (var i in $scope.seasons) {
            if ($window.innerWidth < 620) {
              if (position) {
                $scope.seasons[i].Limit = position[i];
              } else {
                $scope.seasons[i].Limit = 16;
              }
            } else if ($window.innerWidth > 620 && $window.innerWidth < 1024) {
              if (position) {
                $scope.seasons[i].Limit = position[i];
              } else {
                $scope.seasons[i].Limit = 15;
              }
            } else {
              if (position) {
                $scope.seasons[i].Limit = position[i];
              } else {
                $scope.seasons[i].Limit = 16;
              }
            }
          }

        });

        //Get all in background
        dresses.getDressesForSeasons(results, dressLineId).then(function(results) {
          $scope.searchable = true;
          $scope.restLoading = false;
          $scope.seasons = results;

          if(!$localStorage['sotteroOpenSeasons']) {
            $localStorage['sotteroOpenSeasons'] = []
          }

          if(!$localStorage['midgleyOpenSeasons']) {
            $localStorage['midgleyOpenSeasons'] = []
          }

          var position
          if($scope.urlDressLine[1] == 'sottero-and-midgley') {
            position = JSON.parse($localStorage['sotteroOpenSeasons'])
          }
          else if($scope.urlDressLine[1] == 'maggie-sottero') {
            position = JSON.parse($localStorage['midgleyOpenSeasons'])
          }

          for (var i in $scope.seasons) {
            if ($window.innerWidth < 620) {
              if (position) {
                $scope.seasons[i].Limit = position[i];
              } else {
                $scope.seasons[i].Limit = 16;
              }
            } else if ($window.innerWidth > 620 && $window.innerWidth < 1024) {
              if (position) {
                $scope.seasons[i].Limit = position[i];
              } else {
                $scope.seasons[i].Limit = 15;
              }
            } else {
              if (position) {
                $scope.seasons[i].Limit = position[i];
              } else {
                $scope.seasons[i].Limit = 16;
              }
            }
          }
          for (var x in results) {
            for (var i in results[x].dresses) {
              $scope.allDresses.push(results[x].dresses[i]);
            }
          }
          window.prerenderReady = true;
          rememberDressOrder();
        });
      }, 50)

    });

    //Get Advanced Filters
    dresses.getDressLineFilters(dressLineId).then(function(results) {
      var i = results.length;
      while (i--) {
        if (results[i].Name == "Product Type" || results[i].Name == "Discontinued" || results[i].Name == "Loan Sample" || results[i].Name == "Picks" || results[i].Name == "Quiz Type" || results[i].Name == "Season" || results[i].Name == "Priority") {
          results.splice(i, 1);
        }
      }
      results = $filter('orderBy')(results, 'Sequence');
      $scope.advancedFilters = results

      //check to see if category is applied
      if ($routeParams.category) {
        var categories = $routeParams.category.split('&')
        for (var i in results) {
          $scope.filtersActive[i] = [];
          for (var x in results[i].Children) {
            for(var y in categories){
              if (parseInt(categories[y]) == results[i].Children[x].CategoryId) {
                $scope.filtersActive[i][x] = results[i].Children[x].CategoryId;
              }
            }

          }
        }
        $localStorage['activeFilters'] = JSON.stringify($scope.filtersActive);
      }


      //Highlight parents who have active filters
      if ($localStorage['activeFilters']) {
        $scope.filtersActive = JSON.parse($localStorage['activeFilters']);

        //Check active filters
        for (var x in $scope.filtersActive) {
          for (var y in $scope.filtersActive[x]) {

            for (var z in $scope.advancedFilters[x].Children) {
              if ($scope.advancedFilters[x].Children[z].CategoryId == $scope.filtersActive[x][y]) {
                $scope.advancedFilters[x].Children[z].active = true;
              }
            }

          }
        }

      } else {
        for (var x in $scope.advancedFilters) {
          $scope.filtersActive[x] = [];
        }
      }

    })

  };

  //Get Dress line by location
  if ($localStorage['country']) {
    if ($scope.urlDressLine[1] == 'maggie-sottero') {
      $scope.dressLineId = 2;
      $scope.dressLineName = 'Maggie Sottero';

      $('title').html('Maggie Sottero Wedding Dresses & Gowns | Maggie Sottero');
      $('meta[name=title]').attr('content', 'Maggie Sottero Wedding Dresses');
      $('meta[name=description]').attr("content", "For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");

      getDresses(2);
    } else if ($scope.urlDressLine[1] == 'sottero-and-midgley') {
      $scope.dressLineId = 3;
      $scope.dressLineName = 'Sottero and Midgley';

      $('title').html('Sottero and Midgley Wedding Dresses & Gowns | Sottero and Midgley');
      $('meta[name=title]').attr('content', 'Sottero and Midgley Wedding Dresses');
      $('meta[name=description]').attr("content", "Every bride deserves the perfect wedding dress. Regal ball gowns, slinky sheath dresses and sophisticated styling. Find yours at Sottero and Midgley.");

      getDresses(3);
    }
  } else {
    user.getLocationByIP().then(function(response) {
      $localStorage['country'] = response.country_code2;

      if ($scope.urlDressLine[1] == 'maggie-sottero') {
        $scope.dressLineId = 2;
        getDresses(2);
      } else if ($scope.urlDressLine[1] == 'sottero-and-midgley') {
        $scope.dressLineId = 3;
        getDresses(3);
      }
    })
  };

  //short video
  var iframe = $('#player1')[0];
  var player = $f(iframe);

  $scope.showCollectionFilm = function(videoId) {
    $scope.videoSrc = $sce.trustAsResourceUrl('//player.vimeo.com/video/' + videoId + '?api=1&player_id=player1');
    $scope.showVideo = true;
    player.addEvent('ready', function() {
      player.api('play');
    });

  }
  $scope.stopVideo = function() {
    $scope.showVideo = false;
    player.api('pause');
  }

  //Advanced Filters
  $scope.advancedFilterList = [];
  var saveFilters = function() {
    $localStorage['activeFilters'] = JSON.stringify($scope.filtersActive);
    rememberDressOrder();
  };
  if ($localStorage['advancedFilters'] && $localStorage['activeFilters']) {
    $scope.filtersActive = JSON.parse($localStorage['activeFilters']);
  };
  $scope.toggleAdvancedFilter = function(addFilter, filter, parent) {
    if (addFilter) {
      $scope.filtersActive[parent].push(filter);
      saveFilters();
    } else {
      $scope.filtersActive[parent] = $.grep($scope.filtersActive[parent], function(value) {
        return value != filter;
      });
      saveFilters();
    }
  };
  $scope.clearFilters = function() {
    for (var i in $scope.advancedFilters) {
      for (var x in $scope.advancedFilters[i].Children) {
        $scope.advancedFilters[i].Children[x].active = false;
      }
    }
    for (var i in $scope.filtersActive) {
      $scope.filtersActive[i] = [];
    }
    saveFilters();
  };
  $scope.clearSingleFilterList = function(index) {

    //remove active
    for (var i in $scope.advancedFilters[index].Children) {
      $scope.advancedFilters[index].Children[i].active = false;
    }

    //remove active classes
    $scope.filtersActive[index] = [];
    saveFilters();

  };
  $scope.$on('reloadDresses', function() {
    $scope.clearFilters()
  })

  //Hide filters on scroll
  $(document).scroll(function() {
    $(".dropdown.open").removeClass("open");
  })

  //Remember Open Collections
  $scope.rememberOpenSeason = function(index) {
    var limits = [];
    for (var i in $scope.seasons) {
      limits.push($scope.seasons[i].Limit);
    }
    console.log($scope.urlDressLine[1])
    if($scope.urlDressLine[1] == 'sottero-and-midgley') {
      $localStorage['sotteroOpenSeasons'] = JSON.stringify(limits);
    }
    else if($scope.urlDressLine[1] == 'maggie-sottero') {
      $localStorage['midgleyOpenSeasons'] = JSON.stringify(limits);
    }
  };

  //Remember Dress Order
  var rememberDressOrder = function() {
    var dresses = [];
    var hasActiveFilters = _.find($scope.filtersActive, function(f) {
      return f.length;
    })
    if (hasActiveFilters || $scope.searchInput) {
      var filteredDresses = $filter('advancedFilters')($scope.allDresses, $scope.filtersActive);
      filteredDresses = $filter('filter')(filteredDresses, $scope.searchInput);

      for (var i in filteredDresses) {
        var object = {};
        object.ProductId = filteredDresses[i].ProductId;
        object.Name = filteredDresses[i].Name;
        object.DressLine = $scope.urlDressLine[1];
        dresses.push(object);
      }
    } else {
      for (var i in $scope.seasons) {
        for (var x in $scope.seasons[i].dresses) {
          var object = {};
          object.ProductId = $scope.seasons[i].dresses[x].ProductId;
          object.Name = $scope.seasons[i].dresses[x].Name;
          object.DressLine = $scope.urlDressLine[1];
          dresses.push(object);
        }
      }
    }
    $localStorage['dressesOrder'] = JSON.stringify(dresses);

  };
  $scope.$watch('searchInput', function() {
    rememberDressOrder();
  })

  //If has filters applied
  $scope.hasFilters = function() {
    return _.find($scope.filtersActive, function(f) {
      return f.length;
    })
  }

  //Footer Postioning
  var windowHeight = $(window).height();
  $('.page').css('min-height', '' + windowHeight - 355 + 'px');

});
