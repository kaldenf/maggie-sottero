app.controller('aboutUsController', function($scope, $timeout, $window) {
    var windowHeight = $(window).height();
	$('.page').css('min-height', ''+ windowHeight-355 + 'px');
	
	$timeout(function(){
		window.scrollTo(0,0);
	},0);
	
	$('title').html('Bridal Fashion is in Our Blood | About Maggie Sottero');
	$('meta[name=title]').attr('content','Bridal Fashion is in Our Blood | About Maggie Sottero');
	$('meta[name=description]').attr('content','');
	window.prerenderReady = false;
	
});