app.controller('signupController', function($scope, $location, $localStorage, $rootScope, user, facebook, $window, $timeout) {

  $rootScope.hideFooter = true;

  var token = $localStorage['jwtToken'];
  var userId = $localStorage['userId'];

  if (token || userId) {
    $location.path('/home');
  }

  $timeout(function(){
    $( "#datepicker" ).datepicker();
  })

  user.logout();

  $scope.decidingOnSystem = true;

  $scope.form = {};

  $scope.signup = function() {

    if (!$scope.form.zip) {
      $rootScope.$broadcast('notification', 'Please enter a valid zip code');
    } else if (!$scope.form.wDate) {
      $rootScope.$broadcast('notification', 'Please enter a valid wedding date');
    } else if (!$scope.form.username) {
      $rootScope.$broadcast('notification', 'Please enter a valid email');
    } else if (!$scope.form.password) {
      $rootScope.$broadcast('notification', 'Please enter a valid password');
    } else {
      user.register($scope.form.username, $scope.form.password).then(function(response) {
        $scope.form.wDate = moment($scope.form.wDate).toDate();
        console.log($scope.form.wDate)
        //about me information
        var about_me = {
          country: $localStorage['country'],
          zip: $scope.form.zip,
          source: '',
          wdate: $scope.form.wDate,
          name: $scope.form.name
        }
        user.createAboutMe(about_me).then(function() {
          //if user is coming from share wedding
          user.updateAboutMe(about_me).then(function(response) {
            //setting signed in variables
            $rootScope.loggedIn = true;
            $rootScope.userEmail = $scope.username;
            $rootScope.$apply;

            //Add user to newsletter
            user.addToNewsletter().then(function() {
              $location.path('/account');
            })
          })
        });
      });
    }
  }

  $scope.facebookLogin = function() {
    facebook.login().then(function(response) {
      //about me information
      var about_me = {
        country: $localStorage['country'],
        zip: '',
        source: '',
        wdate: '',
        name: response.name
      }

      user.register(response.email, response.id).then(function() {

        //Add user to newsletter
        user.addToNewsletter().then(function() {});

        //Create About me
        user.createAboutMe(about_me).then(function() {
          //if user is coming from share wedding
          if ($rootScope.shareWeddingOnLogin) {
            $location.path('/share-wedding');
            $rootScope.shareWeddingOnLogin = false;
          } else {
            $location.path('/account');
          }

          //set variables for being logged in
          $rootScope.loggedIn = true;
          $rootScope.userEmail = response.email;
          $rootScope.$apply;
          $location.path("/account");
        });
      });
    });
  }

  $('meta[name=description]').attr('content', "For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");
  $('meta[name=title]').attr('content', 'Sign Up For for Tips and Savings | Maggie Sottero');
  document.title = 'Sign Up For for Tips and Savings | Maggie Sottero';
  window.prerenderReady = true;

  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);

});

app.controller('slideshowController', function($scope) {

  $(document).ready(function() {
    $('#slideshow').cycle({
      fx: 'fade',
      pause: 1,
      speed: 1000,
      timeout: 4000
    });
  });

})
