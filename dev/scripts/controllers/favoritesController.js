app.controller('favoritesController', function($scope, $localStorage, $location, user, favorites, $window,  $filter, $rootScope, $timeout, dresses) {

  //Authentication
  var token = $localStorage['jwtToken'];

  if ($localStorage['favorites']) {
    var favorites = JSON.parse($localStorage['favorites']);

    $scope.favorites = [];
    if(favorites.length){
      for (var i in favorites) {
        dresses.getDressById(favorites[i]).then(function(results) {
          results.Images = $filter('filter')(results.Images, 'Medium');
          $scope.favorites.push(results);
        })
      }
    }
    else {
      $scope.noFavorites = true;
    }

  } else {
    if (token) {
      user.getFavorites().then(function(results) {
        if(results.data.length){
          dresses.getDressesById(results.data).then(function(response) {
            for (var i in response) {
              response[i].Images = $filter('filter')(response[i].Images, 'Medium');
            }
            $scope.favorites = response;
          })
        }
        else{
          $scope.noFavorites = true;
        }
      });
    } else {
      $scope.noFavorites = true;
    }
  }

  $scope.removeFavorite = function(dressId) {
    if(token){
      favorites.removeFavorite(dressId).then(function() {
        console.log('removed')
        user.getFavorites().then(function(results) {
          if(results.data.length){
            dresses.getDressesById(results.data).then(function(response) {
              for (var i in response) {
                response[i].Images = $filter('filter')(response[i].Images, 'Medium');
              }
              $scope.favorites = response;
            })
          }
          else{
            $scope.favorites = []
            $scope.noFavorites = true;
          }
        });
      })
    }
    else {
      //function to remove from array
      Array.prototype.remove = function() {
        var what, a = arguments,
          L = a.length,
          ax;
        while (L && this.length) {
          what = a[--L];
          while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
          }
        }
        return this;
      };
      //remove from array and save in local storage
      var StoredFavorites = JSON.parse($localStorage['favorites']);
      StoredFavorites.remove(dressId);
      $localStorage['favorites'] = JSON.stringify(StoredFavorites);
      $rootScope.userFavorites = JSON.parse($localStorage['favorites']);

      //reset favorites for view
      $scope.favorites = [];
      for (var i in StoredFavorites) {
        dresses.getDressById(StoredFavorites[i]).then(function(results) {
          results.Images = $filter('filter')(results.Images, 'Medium');
          $scope.favorites.push(results);
        })
      }
      $rootScope.$apply;
    }
  }

  $scope.removeAll = function() {
    var prompt = confirm("Are you sure you want to delete all your favorites?");
    if (prompt) {
      if(token){
        favorites.removeAllFavorites().then(function() {
          $scope.favorites = []
          $scope.noFavorites = true;
        });
      }
      else {
        $localStorage['favorites'] = [];
        $scope.favorites = [];
        $rootScope.userFavorites = [];
        $scope.noFavorites = true;
      }
    } else {
      return false;
    }
  }

  $('title').html('Favorite Wedding Dresses | Maggie Sottero');

  //Page Resizer
  var windowHeight = $(window).height();
  $('.page').css('min-height', '' + windowHeight - 355 + 'px');

  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);

});
