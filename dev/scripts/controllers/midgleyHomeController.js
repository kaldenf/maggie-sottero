app.controller('midgleyHomeController', function($scope, $rootScope){
	$rootScope.hideFooter = true;

	//SEO
    $('meta[property=og\\:title]').attr('content','Sottero and Midgley Wedding Dresses');
    $('meta[property=og\\:url]').attr('content','http://maggiesottero.com/1');
    $('meta[property=og\\:description]').attr("content','Every bride deserves the perfect wedding dress. Regal ball gowns, slinky sheath dresses and sophisticated styling. Find yours at Sottero and Midgley.");
    $('meta[property=og\\:image]').attr('content','https://ms-cdn.maggiesottero.com/product/Content/Images/59687/Sottero-and-Midgley-Monaco-Spring2016-menu.jpg');

    $('head').append('<link rel="canonical" href="https://www.maggiesottero.com/"/>');
    $('head').append('<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">');


    document.title = 'Sottero and Midgley';

    window.prerenderReady = true;

})
