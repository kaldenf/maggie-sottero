app.controller('mainController', function($scope, banners, $localStorage, dresses, $location, $rootScope, googleMaps, user, maggieBrides, $window, $timeout) {

  $scope.scrollPos = {}; // scroll position of each view

  $(window).on('scroll', function() {
      if ($scope.okSaveScroll) { // false between $routeChangeStart and $routeChangeSuccess
          $scope.scrollPos[$location.path()] = $(window).scrollTop();
          //console.log($scope.scrollPos);
      }
  });

  $scope.scrollClear = function(path) {
      $scope.scrollPos[path] = 0;
  }

  $scope.$on('$routeChangeStart', function() {
      $scope.okSaveScroll = false;
  });

  $scope.$on('$routeChangeSuccess', function() {
      $timeout(function() { // wait for DOM, then restore scroll position
        if($location.path() == '/maggie-sottero' || $location.path() == '/sottero-and-midgley' || $location.path() == '/brides'){
          $(window).scrollTop($scope.scrollPos[$location.path()] ? $scope.scrollPos[$location.path()] : 0);
          $scope.okSaveScroll = true;
        }
      }, 0);
  });



  $scope.focusGlobalSearch = function(){
    setTimeout(function(){
      $('.global-search')[0].focus()
    }, 100)
  }

  $scope.searchSite = function(url){
    $rootScope.showGlobalSearch = false
    window.location.href = url
  }

  $scope.hideGlobalSearch = function(){
    $rootScope.showGlobalSearch = false
    $rootScope.globalSearch = null
  }

  function getMaggieFilters(){
    dresses.getDressLineFilters(2).then(function(data){
      var filters = []
      for(var i in data){
          for(var x in data[i].Children){
            filters.push(data[i].Children[x])
          }
      }
      $scope.maggieFilters = filters
    })
  }

  function getMidgleyFilters(){
    dresses.getDressLineFilters(2).then(function(data){
      var filters = []
      for(var i in data){
          for(var x in data[i].Children){
            filters.push(data[i].Children[x])
          }
      }
      $scope.midgleyFilters = filters
    })
  }

  function getMaggieDresses(){
    dresses.getDressesForDressline(2).then(function(data){
      $scope.maggieDresses = data
    })
  }

  function getMidgleyDresses(){
    dresses.getDressesForDressline(3).then(function(data){
      $scope.midgleyDresses = data
    })
  }

  getMaggieFilters()
  getMidgleyFilters()
  getMidgleyDresses()
  getMaggieDresses()

  //Get banners
  function getBanners(){
    banners.get().then(function(banners) {
      $scope.banners = banners
    })
  }

  //Get banners
  user.getLocationByIP().then(function(response) {
    $localStorage['country'] = response.country_code2
    getBanners()
  })



  //Get url
  $scope.url = $location.absUrl()

  //Authentication
  var token = $localStorage['jwtToken']
  var userId = $localStorage['userId']

  //Var to show links on footer and header if logged in or not
  if (!token || !userId) {
    $rootScope.loggedIn = false
  } else {
    $rootScope.loggedIn = true
  }

  //Get users favorites
  if (token && userId) {
    user.getFavorites().then(function(response) {
      $rootScope.userFavorites = response.data
    })
  } else {
    if ($localStorage['favorites']) {
      $rootScope.userFavorites = JSON.parse($localStorage['favorites'])
    }
  }

  //set Share Data
  $scope.setShareData = function(dress, dressLine) {
    if (dressLine == 'Maggie Sottero') {
      dressLine = 'maggie-sottero'
    } else if (dressLine == 'Sottero & Midgley') {
      dressLine = 'sottero-and-midgley'
    }

    dress.url = 'http://maggiesottero.com/' + dressLine + '/' + dress.Name + '/' + dress.ProductId + ''
    if (dressLine == 'maggie-sottero') {
      dressLine = 'Maggie Sottero'
    } else if (dressLine == 'sottero-and-midgley') {
      dressLine = 'Sottero and Midgley'
    }
    dress.DressLine = dressLine
    if (dress.Image) {
      dress.imagePath = 'https://ms-cdn.maggiesottero.com/product/Content/Images/' + dress.Image.PictureId + '/' + dress.Image.FileName + ''
    } else if (dress.Images) {
      dress.imagePath = 'https://ms-cdn.maggiesottero.com/product/Content/Images/' + dress.Images[0].PictureId + '/' + dress.Images[0].FileName + ''
    }
    dress.dressLine = dressLine
    $scope.socialDress = dress
  }

  $scope.facebookShare = function(){
    FB.ui({
      method: 'feed',
      link: $scope.socialDress.url,
      caption: $scope.socialDress.dressLine + ' - ' + $scope.socialDress.Name,
      description: $scope.socialDress.Description,
      picture: $scope.socialDress.imagePath
    }, function(response){})
  }

  //subscribe to newsletter
  $scope.signupForNewsletter = function() {
    if ($rootScope.newSignUpEmail) {
      user.addUserToNewsletter($scope.newSignUpEmail).then(function() {
        $scope.newSignUpEmail = ''
        $rootScope.$broadcast('notification', 'Signed up for newsletter')
      })
    } else {
      $rootScope.$broadcast('notification', 'Please enter a valid email')
    }
  }

  //Menu animation
  $scope.showMenu = function() {
    $('#main-nav, .collections-select').addClass('show-menu')
    $('body').addClass('no-scroll')
  }
  $scope.closeMenu = function() {
    $('#main-nav, .collections-select').removeClass('show-menu')
    $('body').removeClass('no-scroll')
  }

  $scope.loadSotteroAndMidgley = function() {
    delete $localStorage['advancedFilters']
    delete $localStorage['activeFilters']
    $scope.$broadcast('reloadDresses')
    $location.path('/sottero-and-midgley')
  }
  $scope.loadMaggieSottero = function() {
    delete $localStorage['advancedFilters']
    delete $localStorage['activeFilters']
    $scope.$broadcast('reloadDresses')
    $location.path('/maggie-sottero')
  }

  //Current Path
  $scope.currentPath = $location.path()

  //Notification
  $scope.$on('notification', function(event, message) {
    $scope.notification = message
    $('.notification').addClass('active')
    setTimeout(function() {
      $('.notification').removeClass('active')
    }, 2000)
  })

  //Get Year for copyright
  $scope.year = new Date().getFullYear()

  //Logout
  $scope.logout = function() {
    user.logout()
    $rootScope.loggedIn = false
    $rootScope.$apply
    $location.path('/log-in')
  }

  //Facebook SDK
  window.fbAsyncInit = function() {
    FB.init({
      appId: '1627514134166188',
      xfbml: true,
      version: 'v2.2'
    })
  }

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0]
    if (d.getElementById(id)) {
      return
    }
    js = d.createElement(s)
    js.id = id
    js.src = "//connect.facebook.net/en_US/sdk.js"
    fjs.parentNode.insertBefore(js, fjs)
  }(document, 'script', 'facebook-jssdk'))

  //Location
  $scope.$location = $location

  //clean local storage
  window.onbeforeunload = function() {
    delete $localStorage['advancedFilters']
    delete $localStorage['activeFilters']
    delete $localStorage['maggieOpenSeasons']
    delete $localStorage['midgleyOpenSeasons']
    delete $localStorage['dressesOrder']
  }


})
