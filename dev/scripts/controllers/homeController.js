(function() {
  'use strict'
  angular.module('app').directive('home', homeController)

  function homeController($rootScope) {
    return {

      templateUrl: 'pages/home.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          dressLine: 'maggiesottero'
        })

        $rootScope.hideFooter = true
        $rootScope.noScrollOnPage = true

        //SEO
        $('meta[name=description]').attr('content',"For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");
        $('meta[name=title]').attr('content','Maggie Sottero Wedding Dresses');
        document.title = 'Wedding Dresses and Gowns | Maggie Sottero';
        window.prerenderReady = true;

        //******** end *********//
      }
    }
  }

})();


// app.controller('homeController', function($scope, $rootScope, $window) {
//     $scope.dressLine = 'maggiesottero';
//     $rootScope.hideFooter = true;
//     $rootScope.noScrollOnPage = true;
//
//     //SEO
//     $('meta[name=description]').attr('content',"For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");
//     $('meta[name=title]').attr('content','Maggie Sottero Wedding Dresses');
//     document.title = 'Wedding Dresses and Gowns | Maggie Sottero';
//     window.prerenderReady = true;
//
//
// });
