app.controller('maggieBridesController', function($scope, $rootScope, user, $rootScope, $timeout, maggieBrides) {

	//Get Maggie Bride Posts
  maggieBrides.getPosts().then(function(results) {
    $scope.posts = results.data.posts
  })

	$('#maggie-brides-page').css('opacity','0')

	// setTimeout(function() {
	// 	$rootScope.$broadcast('masonry.reload');
	// }, 1000);

  $timeout(function(){
    $('.grid').masonry({
      // options...
      itemSelector: '.grid-item',
      columnWidth: 60.7
    })
  },1400)

	$rootScope.hideFooter = true;
	$scope.selectedTime = 'Year';
	$scope.selectedDressLine = 'Dress Line';
	$scope.filter = {
		date: '',
		dressLine: ''
	};

	$scope.dressLine = '';
  if($rootScope.bridesCardLimit){
    $scope.cardLimit = 20;
  }
  else {
    $scope.cardLimit = $rootScope.bridesCardLimit;
  }

	$scope.loading = false;
	// setTimeout(function() {
	// 	$(document).scroll(function() {
	// 		if (($(document).height() - $(window).height() - 500) <= $(document).scrollTop() && $scope.loading == false) {
	// 			$scope.loading = true;
	// 			$scope.cardLimit = $scope.cardLimit + 20;
  //       $rootScope.bridesCardLimit = $scope.cardLimit
	// 			$scope.$apply();
	// 			setTimeout(function() {
	// 				$scope.loading = false;
	// 			}, 500);
	// 		}
	// 	})
	// }, 1050);

	//SEO
	document.title = 'Real Wedding Stories from Real Brides | Maggie Sottero';
	$('meta[name=description]').attr('content', '');

	$timeout(function(){
		$('#maggie-brides-page').animate({
			opacity:1
		},500)
	},200);
});
