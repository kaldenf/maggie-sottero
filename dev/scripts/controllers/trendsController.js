app.controller('trendsController', function($scope, $timeout,$location){
	$timeout(function(){
		window.scrollTo(0,0);
	},0);
	
	var url = $location.path().split('/');
	if(url[1] == 'colored-wedding-dresses'){
		document.title = 'Colored Wedding Dresses and Gowns | Maggie Sottero';
		$('meta[name=description]').attr('content',"Brides everywhere are turning to colorful wedding dresses as an alternative to traditional white. See some of our favorite colored wedding dresses.");
	}
	else if (url[1] == 'vintage-wedding-dresses'){
		document.title = 'Vintage Inspired Wedding Dresses and Gowns | Maggie Sottero';
		$('meta[name=description]').attr('content',"Inspired by iconic elements of Old Hollywood, these vintage style wedding dresses call to brides seeking a vintage twist. See all of our vintage dresses.");
	}
	else if (url[1] == 'sleeve-wedding-dresses'){
		document.title = 'Wedding Dresses and Gowns with Sleeves | Maggie Sottero';
		$('meta[name=description]').attr('content',"From intricately beaded sleeve jackets to lace full-sleeves, wedding dresses with sleeves are making a statement. See our favorite sleeve wedding dresses.");
	}
	else if (url[1] == 'lace-wedding-dresses'){
		document.title = 'Lace Wedding Dresses and Gowns | Maggie Sottero';
		$('meta[name=description]').attr('content',"The age old process of lace-making has always brought a special, romantic touch to wedding gowns. See all of our favorite lace wedding dresses.");
	}
	else if (url[1] == 'plus-size-wedding-dresses'){
		document.title = 'Plus Size Wedding Dresses and Gowns | Maggie Sottero';
		$('meta[name=description]').attr('content',"Nothing makes a bride feel more stunning than a perfectly fitting wedding gown. No matter your body shape or type, your dream wedding dress awaits.");
	}
	
	$timeout(function(){
		window.scrollTo(0,0);
	},0);
	
})
