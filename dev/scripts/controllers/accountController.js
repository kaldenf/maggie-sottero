app.controller('accountController', function($scope, $location, $localStorage, user, $window, $rootScope,  $filter, favorites, $timeout) {

  $rootScope.hideFooter = true;

  var token = $localStorage['jwtToken'];
  var userId = $localStorage['userId'];

  if (!token || !userId) {
    $location.path('/log-in');
  }
  //Tabs
  $scope.tab = 'favorites';
  //Get User Info
  user.getInfo().then(function(response) {
    $scope.user = response.data.user;
  });
  //Get Users about me info
  user.getAboutMe().then(function(response) {
    if (response.data !== null) {
      $scope.aboutMe = response.data.about_me;
      $scope.aboutMe.wdate = new Date($scope.aboutMe.wdate);
    }
  });

  //toggle Newsletter
  $scope.toggleNewsletter = function() {
    if ($scope.user.mailing_list == false) {
      user.removeFromNewsletter().then(function(response) {

      })
    } else {
      user.addToNewsletter().then(function(response) {

      })
    }
  }

  //update user
  $scope.saveChanges = function() {
    var user = {
      email: $scope.user.email,
      your_style: $scope.styleId
    }
    if ($scope.password !== "") {
      user.password = $scope.password;
    }
    //Update user
    user.update($scope.user).then(function() {
      user.getInfo().then(function(response) {
        $scope.user = response.data.user;
      });
    })

    if (!$scope.aboutMe.wdate) {
      $rootScope.$broadcast('notification', 'Please enter a wedding date');
    } else {
      if (!$scope.aboutMe.zip) {
        $scope.aboutMe.zip = '';
      }

      //create or update about me information
      user.updateAboutMe($scope.aboutMe).then(function(response) {

      })

      $rootScope.$broadcast('notification', 'Changes Saved');
    }

  }

  //update favorites if any are in local storage
  if ($localStorage['favorites']) {
    var savedFavorites = JSON.parse($localStorage['favorites']);
    for (i = 0; i < savedFavorites.length; i++) {
      favorites.addFavorite(savedFavorites[i]);
    }
    $localStorage['favorites'] = [];
  }

  //Favorites
  user.getFavorites().then(function(results) {
    dresses.getDressesById(results.data).then(function(response) {
      for (var i in response) {
        response[i].Images = $filter('filter')(response[i].Images, 'Medium');
      }
      $scope.favorites = response;
      if (response == undefined) {
        $scope.noFavorites = true;
      }
    })
  });

  $scope.removeAllFavorites = function() {
    var prompt = confirm("Are you sure you want to delete all your favorites?");
    if (prompt) {
      favorites.removeAllFavorites().then(function() {
        user.getFavorites().then(function(results) {
          $scope.favorites = results.data;
        });
      });
    } else {
      return false;
    }
  };

  $scope.removeFavorite = function(dressId) {

    favorites.removeFavorite(dressId).then(function() {
      user.getFavorites().then(function(results) {
        dresses.getDressesById(results.data).then(function(response) {
          for (var i in response) {
            response[i].Images = $filter('filter')(response[i].Images, 'Medium');
          }
          $scope.favorites = response;
        })
      });
    })
  };

  //Your Style
  user.getInfo().then(function(results) {
    if (results.data.user.your_style !== 0) {
      $scope.styleId = results.data.user.your_style;
      switch (parseInt(results.data.user.your_style)) {
        case 1:
          $scope.yourStyle = 'Vintage';
          $scope.styleDescription = 'Inspired by iconic elements of glamorous Old Hollywood, these vintage style wedding dresses call to romantic brides seeking a vintage twist. These retro-inspired beauties glimmer with geometric metallic embellishments, subtle sequins and opalescent pearls. Drawing inspiration from quintessential elements of the 1920s, these vintage wedding dresses encompass Neo-classic columns, Art Deco designs, and jewelry inspired beading. Vintage inspired capelets offer demure coverage with a vintage flair. Read our favorite vintage inspired wedding trends below.';
          break;
        case 2:
          $scope.yourStyle = 'Statement';
          $scope.styleDescription = 'For the bride seeking a statement wedding dress, look no further than these show-stopping beauties. Complete with intricate back detailing, glamorous skirts, and decadent Swarovski crystal beading, statement wedding dresses are absolutely show-stopping. Pair a statement wedding dress with an elegant updo, accented with an avant-garde hairpiece, and a dramatic cat-eye, mirroring the sophistication of your wedding day.';
          break;
        case 3:
          $scope.yourStyle = 'Bohemian';
          $scope.styleDescription = 'From breezy poet sleeves to airy layers of chiffon, bohemian wedding dresses are perfect for the free-spirited bride. These ethereal, whimsical looks are accented by lightweight, barely-there fabrics and streamlined silhouettes. The bohemian bride is laidback and carefree, accenting her wedding with rustic touches… A lush floral crown, natural makeup, and a chunky braid complete your wedding day look.';
          break;
        case 4:
          $scope.yourStyle = 'Glamour';
          $scope.styleDescription = 'From sparkling Swarovski crystal embellishments to breathtaking skirts, dramatic, detailed backs to plunging necklines, glamorous wedding dresses are sure to leave your guests talking about your gown long after the big day. Read our favorite elements of a glamorous wedding dress below. Channel your inner starlet when styling your glamorous wedding dress, choosing contoured cheeks, smoky eyes, and loose curls, parted dramatically to the side.';
          break;
        case 5:
          $scope.yourStyle = 'Classic';
          $scope.styleDescription = "While we're all for modern and fashion-forward weddings, there is something to be said for a celebration that will never go out of style. Aiming for that timeless look? Traditional and elegant, classic wedding dresses stand the test of time. Incorporating traditional elements with romantic details, classic wedding dresses hold a special place in our hearts. See some of our favorites, and read more about achieving your timeless wedding day look below. A sleek bouquet of roses, a chic chignon, and elegant pair of diamond drop earring complement your classic wedding day.";
          break;
        case 6:
          $scope.yourStyle = 'Beach';
          $scope.styleDescription = 'With stunning beaches, endless expanses of white sand, picturesque sunsets, and turquoise water, beach weddings always make us weak in the knees. A sandy beach offers a perfect backdrop for a destination wedding. When planning a beach wedding, choose lightweight fabrics and fuss-free dresses, allowing you to focus on your soon to-be-husband and the sand between your toes. Pair your beach wedding with tousled, relaxed waves, sun-kissed bronzed cheeks, and a subtle coral lip.';
          break;
        default:
          $scope.yourStyle = '';
          $scope.styleDescription = '';
          break;
      }
      $scope.$apply;
    } else {
      $scope.styleId = 0;
    }
  })


  $timeout(function() {
    window.scrollTo(0, 0);
  }, 0);


});
