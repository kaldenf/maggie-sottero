app.controller('storeLocatorController', function($scope,$localStorage,  stores, cities, googleMaps, $window, user, $filter, $timeout) {

	$scope.filter = {}
	$scope.radius = 50
	$scope.storeLimit = 500
	var searchingResults = false

	//Get Stores Near Location
	getStoresNearLocation()

	//Populate Prediction List
	cities.getAddresses().then(function(cities){
		$scope.cities = cities
	})

	//Show all results
	$scope.showAllResults = function() {
		$scope.storeLimit = 1500
		$scope.allResultsShown = true
	}
	//Search Call
	$scope.findStores = function() {
		if ($scope.search) {
			//empty results and show loader
			$scope.searchString = $scope.search
			$scope.storesLoading = true
			$scope.stores = null
			$scope.groups = null
			$scope.hidePrediction = true
			searchingResults = true

			googleMaps.getLocation($scope.search).then(function(results) {
				getStores(results.lng, results.lat)
			})
		}
	}
	//Adjust Search Miles
	$scope.adjustMiles = function(radius){
		$scope.radius = radius
		if(!$scope.search && $scope.search.length == 0){
			getStoresNearLocation()
		}
		else{
			$scope.findStores()
		}
	}

	$scope.showAllUSAStores = function(){
		$scope.storesLoading = true
		$scope.stores = null
		$scope.groups = null
		$scope.groupSearch = ''
		$scope.groupCategory = 'Select State'
		$scope.search = ''
		stores.getInCountry(1).then(function(stores){
			stores = $filter('orderBy')(stores, 'Name')
			for(var i in stores){
				stores[i].Stores = cleanStoreObject(stores[i].Stores)
			}
			$scope.groups = stores
			$scope.storesLoading = false
		})
	}

	$scope.showAllInternationalStores = function(){
		$scope.storesLoading = true
		$scope.stores = null
		$scope.groups = null
		$scope.groupSearch = ''
		$scope.groupCategory = 'Select Country'
		$scope.search = ''
		stores.getInternationalStores().then(function(stores){
			stores = $filter('orderBy')(stores, 'Name')
			for(var i in stores){
				stores[i].Stores = cleanStoreObject(stores[i].Stores)
			}
			console.log(stores)
			$scope.groups = stores
			$scope.storesLoading = false
		})
	}

	$scope.expandSearch = function(){
		$scope.radius = $scope.radius*2
		if(!$scope.search && $scope.search.length == 0){
			getStoresNearLocation()
		}
		else{
			$scope.findStores()
		}
	}

	$scope.adjustGroupSearch = function(group){
		$scope.groupSearch = group
	}

	//Get Map
	$scope.hideMap = function(){
		$scope.showMap = false
	}
	function initMap(center, markers) {
	  // Create a map object and specify the DOM element for display.
	  var map = new google.maps.Map(document.getElementById('map'), {
	    center: center,
	    scrollwheel: false,
	    zoom: 8
	  });
		var infowindow = new google.maps.InfoWindow();
		for(var i in markers){
			var marker = new google.maps.Marker({
				position:markers[i].position,
				map:map
			})
			google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
          infowindow.setContent(markers[i].title);
          infowindow.open(map, marker);
        }
      })(marker, i));

      google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
        return function() {
          infowindow.close(map, marker);
        }
      })(marker, i));
		}
	}

	//Scope Helper Functions
	$scope.greaterThan = function(prop, val){
		if(prop){
			if(prop.length > val){
				return prop
			}
			else {
				return null
			}
		}
	}

	$scope.setSearch = function(city) {
		$scope.search = city.City +', '+ city.State +', '+ city.CountryName
		$scope.hidePrediction = true
		$scope.findStores()
	}

	//Truncate Titles on mobile
	$scope.truncateSize = 100
	if ($window.innerWidth < 620) {
		$scope.truncateSize = 21
	}

	//Set Window Height
	var windowHeight = $(window).height()
	$('.page').css('min-height', '' + windowHeight - 355 + 'px')
	$('meta[name=description]').attr('content',"For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.")
    $('meta[name=title]').attr('content','Find a Store Near You | Maggie Sottero ')
    document.title = 'Find a Store Near You | Maggie Sottero'
    window.prerenderReady = true

    $timeout(function(){
		window.scrollTo(0,0)
	},0)

	//Search UX
	$('#search').keypress(function(){
		$scope.hidePrediction = false
	})
	$(document).mouseup(function (e){
	    var container = $(".prediction-box")

	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        $scope.hidePrediction = true
	        $scope.$apply()
	    }
	})

	//Helper Functions
	function cleanStoreObject(stores){
		for(var i in stores){
			stores[i].Premier = false
			for( var x in stores[i].ProductLines){
				var premier = stores[i].ProductLines[x].Premier
				if(premier > 0){
					stores[i].Premier = true
				}
			}
		}
		return stores
	}

	function getLocationByIP(){
		$scope.$apply(function(){
			$scope.stores = []
			$scope.storesLoading = false
			$scope.locationNotFound = true
		})
		// user.getLocationByIP().then(function(response){
		// 	$localStorage['country'] = response.country_code2
		// 	getStores(response.longitude, response.latitude)
		// })
	}

	function getStores(long, lat){
		stores.get(long, lat, $scope.radius).then(function(stores){

			var center = {lat: lat, lng: long}
			var getMarkers = function(){
				var array = []
				for(var i in stores){
					var object = {
						position:{
							lat:stores[i].Latitude,
							lng:stores[i].Longitude
						},
						title:stores[i].Name
					}
					array.push(object)
				}
				return array
			}
			initMap(center, getMarkers())


			$scope.stores = cleanStoreObject(stores)
			$scope.storesLoading = false
		})
	}

	function getStoresNearLocation(){
		$scope.search = ''
		$scope.stores = null
		$scope.groups = null
		$scope.storesLoading = true
		if ("geolocation" in navigator) {
		  $window.navigator.geolocation.getCurrentPosition(function(results){
				if(!searchingResults){
					googleMaps.getAddress(results.coords.longitude, results.coords.latitude).then(function(location){
						$localStorage['country'] = location.results[0].address_components[5].short_name
					})
					stores.get(results.coords.longitude, results.coords.latitude, $scope.radius).then(function(stores){
						var center = {lat: results.coords.latitude, lng: results.coords.longitude}
						var getMarkers = function(){
							var array = []
							for(var i in stores){
								var object = {
									position:{
										lat:stores[i].Latitude,
										lng:stores[i].Longitude
									},
									title:stores[i].Name
								}
								array.push(object)
							}
							return array
						}
						initMap(center, getMarkers())

						if(!searchingResults){
							$scope.stores = cleanStoreObject(stores)
							$scope.storesLoading = false
							$scope.yourLocation = true
						}
					})
				}
			}, function(error){
				getLocationByIP()
			})
		}
		else {
			getLocationByIP()
		}
	}


})
