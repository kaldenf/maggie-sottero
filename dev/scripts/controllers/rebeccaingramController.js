(function() {
  'use strict'
  angular.module('app').directive('rebeccaingram', ingramController)

  function ingramController($rootScope, user, $timeout, $q) {
    return {

      templateUrl: 'pages/rebeccaingram.html',
      controllerAs: 'vm',
      controller: function() {
        var vm = this

        angular.extend(vm, {
          //Variables and Functions
          signup: signup,
          email: null,
          message: null,
          showVideo: false,
          playVideo: playVideo,
          stopVideo: stopVideo,
          shortVideoReady: false,
          videosrc: 'https://player.vimeo.com/external/174849722.hd.mp4?s=e80e0a8da599191d9633d9711c29eaf384c3b5e3&profile_id=174'
        })

        $rootScope.hideheader = true
        detectEndVideo()

        $timeout(function(){
          vm.shortVideoReady = true
        },500)

        function signup() {
          if(vm.email){
            console.log(vm.email)
            return $q.when(user.addUserToNewsletter(vm.email).then(function(){
              vm.email = null
              vm.message = "Your email has been signed up"
            }))
          }
        }

        function playVideo(){
          if(window.innerWidth < 800){
            $('#video')[0].src = 'https://player.vimeo.com/external/174849722.sd.mp4?s=7a389f4516b8aa888a73aca71265df783f8cc829&profile_id=164'
            $('#video')[0].load()
            vm.showVideo = true
          } else {
            vm.showVideo = true
          }
          $('#video')[0].play()
        }

        function stopVideo(){
          vm.showVideo = false
          $timeout(function(){
            $('#video')[0].pause()
            $('#video')[0].currentTime = 0
          },650)
        }

        function detectEndVideo() {
          $('#video').on("ended", function(){
            console.log('ended')
            stopVideo()
          })
        }


        //SEO
        $('meta[name=description]').attr('content',"For every bride, there is a perfect wedding dress waiting to be discovered. Romantic ball gowns, chic sheath dresses, form-fitting mermaid gowns... it's all here at Maggie Sottero. Come along with us... your fairytale awaits.");
        $('meta[name=title]').attr('content','Maggie Sottero Wedding Dresses');
        document.title = 'Wedding Dresses and Gowns | Maggie Sottero';
        window.prerenderReady = true;

        //******** end *********//
      }
    }
  }

})();
