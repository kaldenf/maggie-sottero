app.controller('contactController', function($scope, user, $rootScope, $timeout) {
    var windowHeight = $(window).height();
	$('.page').css('min-height', ''+ windowHeight-355 + 'px');

	$rootScope.hideFooter = true;
	$scope.selectedSender = 'I\'m an authorized retailer';

	var clearFormData = function(){
		$scope.retailerEmail = '';
		$scope.retailerName = '';
		$scope.retailerMessage = '';
		$scope.retailerStoreName = '';
		$scope.retailerState = '';
    $scope.retailerPhoneNumber = '';

		$scope.potRetailerEmail = '';
		$scope.potRetailerName = '';
		$scope.potRetailerMessage = '';
		$scope.potRetailerStoreName = '';
		$scope.potRetailerState = '';
    $scope.potRetailerPhoneNumber = '';

		$scope.mediaEmail = '';
		$scope.mediaName = '';
		$scope.mediaMessage = '';
		$scope.mediaOutlet = '';

		$scope.otherName = '';
		$scope.otherMessage = '';
	}

	//Send Retailer Message
	$scope.sendRetailerEmail = function(){
		if($scope.retailerEmail && $scope.retailerName && $scope.selectedSender && $scope.retailerMessage && $scope.retailerStoreName && $scope.retailerCity && $scope.retailerCountry && $scope.retailerPhoneNumber){
			var message;
			if($scope.retailerState){
				message = 'My name is '+$scope.retailerName+' and my store name is '+$scope.retailerStoreName+', located in  '+$scope.retailerCity+', '+$scope.retailerState+', '+$scope.retailerCountry+'. '+$scope.retailerMessage+', and my phone number is '+$scope.retailerPhoneNumber+'';
			}
			else {
				message = 'My name is '+$scope.retailerName+' and my store name is '+$scope.retailerStoreName+', located in '+$scope.retailerCity+', '+$scope.retailerCountry+'. '+$scope.retailerMessage+', and my phone number is '+$scope.retailerPhoneNumber+'';
			}
			if($scope.retailerCountry == 'United States of America'){
				if(!$scope.retailerState){
					$rootScope.$broadcast('notification','Please select a state');
				}
				else {
					$scope.emailSending = true;
					user.sendContactForm($scope.retailerEmail, $scope.retailerName, $scope.selectedSender, message).then(function(){
						$scope.emailSending = false;
						$scope.showThankYouPage = true;
						clearFormData();
					});
				}
			}
			else {
				$scope.emailSending = true;
				user.sendContactForm($scope.retailerEmail, $scope.retailerName, $scope.selectedSender, message).then(function(){
					$scope.emailSending = false;
					$scope.showThankYouPage = true;
					clearFormData();
				});
			}
		}
		else if(!$scope.retailerMessage){
			$rootScope.$broadcast('notification','Please enter a message');
		}
		else if(!$scope.retailerStoreName){
			$rootScope.$broadcast('notification','Please enter a store name');
		}
		else if(!$scope.retailerCity){
			$rootScope.$broadcast('notification','Please enter a state or city');
		}
		else if(!$scope.retailerCountry){
			$rootScope.$broadcast('notification','Please enter a country');
		}
    else if(!$scope.retailerPhoneNumber){
      $rootScope.$broadcast('notification','Please enter a phone number');
    }
	}

	$scope.retailerNextAction = function(){
		if(!$scope.retailerName){
			$rootScope.$broadcast('notification','Please enter a senders name');
		}
		else if(!$scope.retailerEmail){
			$rootScope.$broadcast('notification','Please enter a valid email');
		}
		else {
			$scope.retailerNext = true;
		}
	}

	//Send Potential Retailer Message
	$scope.sendPotentialRetailerEmail = function(){
		if($scope.potRetailerEmail && $scope.potRetailerName && $scope.selectedSender && $scope.potRetailerMessage && $scope.potRetailerStoreName && $scope.potRetailerCity && $scope.potRetailerCountry && $scope.potRetailerPhoneNumber){
			var interest;
			if($scope.potRetailerMaggieInterest){
				if($scope.potRetailerMidgleyInterest){
					interest = 'I am interested in Maggie Sottero and Sottero and Midgley dress lines.';
				}
				else {
					interest = 'I am interested in the Maggie Sottero dress line.';
				}
			}
			else if($scope.potRetailerMidgleyInterest) {
				interest = 'I am interested in the Sottero and Midgley dress line.';
			}
			var message;
			if($scope.potRetailerState){
				message = 'My name is '+$scope.potRetailerName+' and my store name is '+$scope.potRetailerStoreName+', located in '+$scope.potRetailerCity+', '+$scope.potRetailerState+', '+$scope.potRetailerCountry+'. '+interest+' '+$scope.potRetailerMessage+', and my phone number is '+$scope.potRetailerPhoneNumber+'';
			}
			else {
				message = 'My name is '+$scope.potRetailerName+' and my store name is '+$scope.potRetailerStoreName+', located in '+$scope.potRetailerCity+', '+$scope.potRetailerCountry+'. '+interest+' '+$scope.potRetailerMessage+', and my phone number is '+$scope.potRetailerPhoneNumber+'';
			}

			if($scope.potRetailerCountry == 'United States of America'){
				if(!$scope.potRetailerState){
					$rootScope.$broadcast('notification','Please select a state');
				}
				else {
					$scope.emailSending = true;
					user.sendContactForm($scope.potRetailerEmail, $scope.potRetailerName, $scope.selectedSender, message).then(function(){
						$scope.showThankYouPage = true;
						$scope.emailSending = false;
						clearFormData();
					});
				}
			}
			else {
				$scope.emailSending = true;
				user.sendContactForm($scope.potRetailerEmail, $scope.potRetailerName, $scope.selectedSender, message).then(function(){
					$scope.showThankYouPage = true;
					$scope.emailSending = false;
					clearFormData();
				});
			}

		}
		else if(!$scope.potRetailerMessage){
			$rootScope.$broadcast('notification','Please enter a message');
		}
		else if(!$scope.potRetailerStoreName){
			$rootScope.$broadcast('notification','Please enter a store name');
		}
		else if(!$scope.potRetailerCity){
			$rootScope.$broadcast('notification','Please enter a state or city');
		}
		else if(!$scope.potRetailerCountry){
			$rootScope.$broadcast('notification','Please enter a country');
		}
    else if(!$scope.potRetailerPhoneNumber){
      $rootScope.$broadcast('notification','Please enter a phone number');
    }

	}

	$scope.potentialRetailerNextAction = function(){
		if(!$scope.potRetailerName){
			$rootScope.$broadcast('notification','Please enter a senders name');
		}
		else if(!$scope.potRetailerEmail){
			$rootScope.$broadcast('notification','Please enter a valid email');
		}
		else {
			$scope.potentialRetailerNext = true;
		}
	}

	//Send Media Email
	$scope.sendMediaEmail = function(){
		if($scope.mediaOutlet && $scope.mediaMessage){
			var message = 'My name is '+$scope.mediaName+' and my media outlet is '+$scope.mediaOutlet+'. '+$scope.mediaMessage+'';
			$scope.emailSending = true;
			user.sendContactForm($scope.mediaEmail, $scope.mediaName, $scope.selectedSender, message).then(function(){
				clearFormData();
				$scope.showThankYouPage = true;
				$scope.emailSending = false;
			});
		}
		else if(!$scope.mediaOutlet){
			$rootScope.$broadcast('notification','Please enter a media outlet');
		}
		else if(!$scope.mediaMessage){
			$rootScope.$broadcast('notification','Please enter a message');
		}
	}

	$scope.mediaNextAction = function(){
		if(!$scope.mediaName){
			$rootScope.$broadcast('notification','Please enter a senders name');
		}
		else if(!$scope.mediaEmail){
			$rootScope.$broadcast('notification','Please enter a valid email');
		}
		else {
			$scope.mediaNext = true;
		}
	}

	//Send Other Email
	$scope.sendOtherEmail = function(){
		if($scope.otherMessage){
			var message = 'My name is '+$scope.otherName+'. '+$scope.otherMessage+'';
			$scope.emailSending = true;
			user.sendContactForm($scope.otherEmail, $scope.otherName, $scope.selectedSender, message).then(function(){
				$scope.showThankYouPage = true;
				$scope.emailSending = false;
				clearFormData();
			});
		}
		else if(!$scope.otherMessage){
			$rootScope.$broadcast('notification','Please enter a message');
		}
	}

	$scope.otherNextAction = function(){
		if(!$scope.otherName){
			$rootScope.$broadcast('notification','Please enter a senders name');
		}
		else if(!$scope.otherEmail){
			$rootScope.$broadcast('notification','Please enter a valid email');
		}
		else {
			$scope.otherNext = true;
		}
	}

	$('title').html('Contact Us | Maggie Sottero');
	$('meta[name=title]').attr('content', 'Contact Us | Maggie Sottero');
    $('meta[name=description]').attr('content','');
    window.prerenderReady = true;

    $timeout(function(){
		window.scrollTo(0,0);
	},0);


});
