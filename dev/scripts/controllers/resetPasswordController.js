app.controller('resetPasswordController', function($scope, $location, user, $routeParams, $rootScope, $timeout){
	
	var token = $routeParams.token;
	
	$scope.resetPassword = function(){
		user.resetPassword(token, $scope.newPassword).then(function(){
			$rootScope.loggedIn = true;
			$rootScope.$apply;
			$location.path('/account');
		})
	}
	
	$timeout(function(){
		window.scrollTo(0,0);
	},0);
	
})