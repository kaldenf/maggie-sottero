app.service('stores', function($http, $q) {

  var storeapi = '//ms-api.maggiesottero.com/store/';

  this.get = function(lng, lat, milesRadius) {
    var defferer = $q.defer();
    var range = milesRadius;

    $http.get(storeapi + 'api/stores?latitude=' + lat + '&longitude=' + lng + '&radius=' + range + '', {
      cache: 'true'
    }).success(function(data) {
      var stores = []
      for (var i in data) {
        stores.push(data[i]);
      }
      stores = _.uniq(stores, 'StoreId')
      defferer.resolve(stores);
    }).error(function(error) {

    });
    return defferer.promise;
  };

  this.getInCountry = function(countryId) {
    var defferer = $q.defer();
    var stores = [];
    $http.get(storeapi + 'api/stores?merchantId=1&productLineId=2&countryId=' + countryId + '&state=', {
      cache: 'true'
    }).success(function(data) {
      for (var i in data) {
        stores.push(data[i])
      }
      $http.get(storeapi + 'api/stores?merchantId=1&productLineId=3&countryId=' + countryId + '&state=', {
        cache: 'true'
      }).success(function(data) {
        for (var i in data) {
          stores.push(data[i])
        }

        //Take out duplicates
        var uniqueList = _.uniq(stores, 'StoreId')
          //Group by states
        var storesByState = _.groupBy(uniqueList, 'State')
        var array = []
        for (var key in storesByState) {
          var object = {
            Name: key,
            Stores: storesByState[key]
          }
          array.push(object)
        }
        defferer.resolve(array);
      }).error(function(error) {});
    }).error(function(error) {});
    return defferer.promise;
  }

  this.getInternationalStores = function() {
    var deferred = $q.defer()
    $http.get(storeapi + 'api/stores?latitude=0&longitude=0&radius=12500', {
      cache: 'true'
    }).success(function(data) {
      var stores = _.remove(data, function(store) {
        return store.Country !== "United States"
      })
      var storesByCountry = _.groupBy(stores, 'Country')
      var array = []
      for (var key in storesByCountry) {
        var object = {
          Name: key,
          Stores: storesByCountry[key]
        }
        array.push(object)
      }
      deferred.resolve(array)
    })
    return deferred.promise
  }


})
