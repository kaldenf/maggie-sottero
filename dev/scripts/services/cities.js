app.service('cities', function($http, $window, $q, $localStorage, favorites) {

  var storeapi = '//ms-api.maggiesottero.com/store/';

  this.getAddresses = function() {
    var deferred = $q.defer()
    $http.get(storeapi + 'api/storeAddresses', {
      cache: 'true'
    }).success(function(data) {
      deferred.resolve(data)
    })
    return deferred.promise
  }


})
