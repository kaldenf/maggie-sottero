app.factory('$localStorage', function($window) {

  var tempStorage = {}

  try {
    localStorage.setItem('test', true);
    localStorage.removeItem('test');
  } catch (e) {
    return tempStorage
  }

  return $window.localStorage

})
