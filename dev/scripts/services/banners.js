app.service('banners', function($http, $q, $localStorage){

  var storeapi = '//ms-api.maggiesottero.com/store/';

  this.get = function() {
    var defferer = $q.defer();
    var banners = {};
    var countryIso = $localStorage['country'];
    if (countryIso) {
      $http.get(storeapi + 'api/countries/' + countryIso + '/collections', {
        cache: 'true'
      }).success(function(data) {
        var banners = {}

        for (var i in data) {
          if (data[i].ProductLineId == 2 && data[i].Sequence == 1) {
            for (var x in data[i].Images) {
              if (data[i].Images[x].Tag == "Collection-Home") {
                banners.maggie = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Menu") {
                banners.maggieMenu = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Mobile") {
                banners.maggiePhoneBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Tablet") {
                banners.maggieTabletBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Find-Your-Style-Tablet") {
                banners.styleTabletBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Find-Your-Style-Mobile") {
                banners.stylePhoneBanner = data[i].Images[x];
              }
            }
            break;
          } else if (data[i].ProductLineId == 2 && data[i].Sequence == 2) {
            for (var x in data[i].Images) {
              if (data[i].Images[x].Tag == "Collection-Home") {
                banners.maggie = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Menu") {
                banners.maggieMenu = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Mobile") {
                banners.maggiePhoneBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Tablet") {
                banners.maggieTabletBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Find-Your-Style-Tablet") {
                banners.styleTabletBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Find-Your-Style-Mobile") {
                banners.stylePhoneBanner = data[i].Images[x];
              }
            }
            break;
          }
        }
        for (var i in data) {
          if (data[i].ProductLineId == 3 && data[i].Sequence == 101) {
            for (var x in data[i].Images) {
              if (data[i].Images[x].Tag == "Collection-Home") {
                banners.midgley = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Find-Your-Style") {
                banners.style = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Menu") {
                banners.midgleyMenu = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Tablet") {
                banners.midgleyTabletBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Mobile") {
                banners.midgleyPhoneBanner = data[i].Images[x];
              }
            }
            break;
          } else if (data[i].ProductLineId == 3 && data[i].Sequence == 102) {
            for (var x in data[i].Images) {
              if (data[i].Images[x].Tag == "Collection-Home") {
                banners.midgley = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Find-Your-Style") {
                banners.style = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Menu") {
                banners.midgleyMenu = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Tablet") {
                banners.midgleyTabletBanner = data[i].Images[x];
              } else if (data[i].Images[x].Tag == "Collection-Home-Mobile") {
                banners.midgleyPhoneBanner = data[i].Images[x];
              }
            }
            break
          }
        }
        defferer.resolve(banners);
      }).error(function(error) {});
    } else {
      defferer.resolve(false);
    }
    return defferer.promise;
  }

})
