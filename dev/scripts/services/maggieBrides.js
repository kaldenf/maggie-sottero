app.service('maggieBrides', function($http, $q, $localStorage) {

  var api = 'https://cms-api.maggiesottero.com';

  this.getPosts = function() {
    var deferred = $q.defer();

    $http.get(api + '/api/v1/posts/approved', {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.getToken = function() {
    var deferred = $q.defer();

    $http.get(api + '/api/v1/posts/api_token').then(function(response) {
      var token = response.data.token
      $localStorage['brideToken'] = token
      deferred.resolve(true)
    }, function(error) {

    })

    return deferred.promise;
  }

  this.submitPost = function(email, name, dress_details, about_wedding, images) {
    var deferred = $q.defer();

    var token = $localStorage['brideToken'];

    $http.post(api + '/api/v1/posts/', {
      email: email,
      name: name,
      dress_details: dress_details,
      about_wedding: about_wedding,
      images: images
    }, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.getPost = function(id) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];

    $http.get(api + '/api/v1/posts/approved/' + id, {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.getPostByDress = function(id) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];

    $http.get(api + '/api/v1/posts/approved/dress_id/' + id, {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

});
