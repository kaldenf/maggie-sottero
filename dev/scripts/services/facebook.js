app.service('facebook', function($q, $localStorage) {

  this.login = function() {
    var deferred = $q.defer();
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        // Do nothing
      } else {
        FB.login(function(response) {
          if (response.status == "connected") {
            FB.api('/me?fields=name,email', function(response) {
              if (response && !response.error) {
                deferred.resolve(response);
              } else {

              }
            });
          }
        }, {
          scope: 'email, user_photos'
        });
      }
    });
    return deferred.promise;
  };

  this.getAlbums = function() {
    var deferred = $q.defer();

    var getAlbums = function() {
      FB.api('/me/albums', {
        fields: 'id,name,count,cover_photo,picture'
      }, function(response) {
        var albums = [];
        for (var i = 0; i < response.data.length; i++) {
          var album = {
            'name': response.data[i].name,
            'url': response.data[i].picture.data.url,
            'id': response.data[i].id
          }
          albums.push(album);
        }
        deferred.resolve(albums);
      });
    }

    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        getAlbums();
      } else {
        FB.login(function(response) {
          if (response.status == "connected") {
            getAlbums();
          }
        }, {
          scope: 'user_photos'
        });
      }
    });

    return deferred.promise;
  }

  this.getAlbum = function(id) {
    var deferred = $q.defer();

    var getAlbum = function(id) {
      FB.api('/' + id + '/photos', {
        fields: 'id,name,images'
      }, function(response) {
        var photos = [];

        for (var i = 0; i < response.data.length; i++) {
          var sourceLength = response.data[i].images.length;
          var photo = {
            'name': response.data[i].name,
            'url': response.data[i].images[sourceLength - 1].source,
            'source': response.data[i].images[0].source,
            'id': response.data[i].id
          }
          photos.push(photo);
        }
        deferred.resolve(photos);
      });
    }

    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        getAlbum(id);
      } else {
        FB.login(function(response) {
          if (response.status == "connected") {
            getAlbum(id);
          }
        }, {
          scope: 'user_photos'
        });
      }
    });

    return deferred.promise;
  }

})
