app.service('marketingData', function($http, $q, $localStorage) {

  this.getMarketingArray = function() {
    var deferred = $q.defer();

    //Get marketing data from s3 bucket
    $http.get('https://s3-us-west-2.amazonaws.com/maggiesotterocms/data.json', {
      cache: 'true'
    }).success(function(data) {

      deferred.resolve(data);
    })

    return deferred.promise;
  };

});
