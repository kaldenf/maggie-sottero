app.service('dresses', function($http, $window, $q, $localStorage, favorites) {

  var storeapi = '//ms-api.maggiesottero.com/store/';
  var productapi = '//ms-api.maggiesottero.com/product/';


  this.getSeasons = function(productLine) {
    var countryIso = $localStorage['country'];
    var defferer = $q.defer();
    $http.get(storeapi + 'api/countries/' + countryIso + '/productlines/' + productLine + '/collections', {
        cache: 'true'
      })
      .success(function(data) {
        defferer.resolve(data);
      })
      .error(function(error) {

      });
    return defferer.promise;

  };

  this.getDressesForSeasons = function(seasons, productLine) {
    var defferer = $q.defer();
    var currentRequest = 0;
    var countryIso = $localStorage['country'];


    for (var i in seasons) {
      for (var x in seasons[i].Images) {
        if (seasons[i].Images[x].Tag == "Collection-Banner") {
          seasons[i].collectionBanner = seasons[i].Images[x];
        } else if (seasons[i].Images[x].Tag == "Collection-Logo-Full") {
          seasons[i].collectionBannerLogo = seasons[i].Images[x];
        }
      }
    }

    function getDressesForSeason() {
      $http.get(storeapi + 'api/countries/' + countryIso + '/Collections/' + seasons[currentRequest].CollectionId + '/products', {
        cache: 'true'
      }).success(function(data) {
        seasons[currentRequest].dresses = data;

        for (var i in data) {
          if (favorites.checkIfFavorited(data[i].ProductId)) {
            seasons[currentRequest].dresses[i].favorited = true;
          };
        }

        currentRequest++;
        if (currentRequest < seasons.length) {
          getDressesForSeason();
        } else {
          defferer.resolve(seasons);
        }
      });
    }

    getDressesForSeason();
    return defferer.promise;
  };

  this.getDressesForSeason = function(seasonId, productLine) {
    var defferer = $q.defer();
    var currentRequest = 0;
    var countryIso = $localStorage['country'];
    $http.get(storeapi + 'api/countries/' + countryIso + '/Collections/' + seasonId + '/products', {
      cache: 'true'
    }).success(function(data) {
      var dresses = data;

      for (var i in dresses) {
        if (favorites.checkIfFavorited(dresses[i].ProductId)) {
          dresses[i].favorited = true;
        };
      }

      defferer.resolve(dresses);
    });

    return defferer.promise;
  };

  this.getDressesForDressline = function(dressLineId) {
    var defferer = $q.defer();

    $http.get(storeapi + 'api/countries/usa/productlines/' + dressLineId + '/products?categories=420&limit=', {
      cache: 'true'
    }).success(function(data) {
      defferer.resolve(data);
    }).error(function(error) {

    });

    return defferer.promise;
  }

  this.getDressById = function(dressId) {
    var defferer = $q.defer();

    $http.get(productapi + 'api/merchants/1/products/' + dressId, {
      cache: 'true'
    }).success(function(data) {
      defferer.resolve(data);
    }).error(function(error) {

    });

    return defferer.promise;
  };

  this.getDressesById = function(dresses) {
    var defferer = $q.defer();
    var currentRequest = 0;
    var dressDetails = [];

    function getDresseDetails() {
      $http.get(productapi + 'api/merchants/1/products/' + dresses[currentRequest], {
        cache: 'true'
      }).success(function(data) {
        dressDetails[currentRequest] = data;
        currentRequest++;
        if (currentRequest < dresses.length) {
          getDresseDetails();
        } else {
          defferer.resolve(dressDetails);
        }
      });
    }
    getDresseDetails();
    return defferer.promise;
  };

  this.getDressLineFilters = function(productLine) {
    var defferer = $q.defer();
    var countryIso = $localStorage['country'];

    $http.get(storeapi + 'api/countries/' + countryIso + '/Categories?productLineId=' + productLine + '', {
      cache: 'true'
    }).success(function(data) {
      data = _.remove(data, function(item){
        return item.Name !== "Product Type" && item.Name !== "Discontinued" && item.Name !== "Loan Sample" && item.Name !== "Picks" && item.Name !== "Quiz Type" && item.Name !== "Season" && item.Name !== "Priority"
      })
      defferer.resolve(data)
    }).error(function(error) {

    });

    return defferer.promise;
  };

  this.getDressesByCategory = function(productLineId, categoryId) {
    var defferer = $q.defer();
    var countryIso = $localStorage['country'];
    $http.get(storeapi + 'api/countries/' + countryIso + '/ProductLines/' + productLineId + '/products?categories=' + categoryId + '&limit=12', {
      cache: 'true'
    }).success(function(data) {
      defferer.resolve(data);
    }).error(function(error) {

    });

    return defferer.promise;
  };

  this.getRelatedDresses = function(productId) {
    var defferer = $q.defer();
    $http.get(productapi + 'api/merchants/1/products/' + productId + '/relatedproducts', {
      cache: 'true'
    }).success(function(data) {
      defferer.resolve(data);
    }).error(function(error) {

    });
    return defferer.promise;
  }

  this.getMaggiePicks = function() {
    var defferer = $q.defer();
    var countryIso = $localStorage['country'];
    var maggiePicks = [];
    $http.get(storeapi + 'api/countries/' + countryIso + '/productlines/2/products?categories=489', {
      cache: 'true'
    }).success(function(data) {
      for (var i in data) {
        data[i].dressline = 'maggie-sottero';
        maggiePicks.push(data[i]);
      }
      $http.get(storeapi + 'api/countries/' + countryIso + '/productlines/3/products?categories=489', {
        cache: 'true'
      }).success(function(data) {
        for (var i in data) {
          data[i].dressline = 'sottero-and-midgley';
          maggiePicks.push(data[i]);
        }
        defferer.resolve(maggiePicks);
      }).error(function(error) {
        defferer.resolve(error);
      });
    }).error(function(error) {
      defferer.resolve(error);
    });
    return defferer.promise;
  }

  this.addDressToRecentlyViewed = function(id) {
    var recentlyViewed = [];
    if ($localStorage['recentlyViewed']) {
      recentlyViewed = JSON.parse($localStorage['recentlyViewed']);
    }
    var length = recentlyViewed.length - 1;
    if (recentlyViewed[length] !== id) {
      recentlyViewed.push(id);
    }
    $localStorage['recentlyViewed'] = JSON.stringify(recentlyViewed);
  };

  this.getRecentlyViewed = function() {
    var deferred = $q.defer();

    if ($localStorage['recentlyViewed']) {
      var recentlyViewed = JSON.parse($localStorage['recentlyViewed'])
      var length = recentlyViewed.length;
      var recentlyViewed = recentlyViewed.splice(length - 3, 2);
      deferred.resolve(recentlyViewed);
    } else {
      deferred.resolve(true);
    }

    return deferred.promise;
  }

});
