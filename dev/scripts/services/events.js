app.service('events', function($http, $q) {

  var storeapi = '//ms-api.maggiesottero.com/store/';

  this.get = function(lng, lat, radius) {
    var defferer = $q.defer();
    var range = radius;
    var events = []
    var responses = [];
    $http.get(storeapi + 'api/days/365/events?productLineId=2&latitude=' + lat + '&longitude=' + lng + '&radius=' + range, {
      cache: 'true'
    }).success(function(data) {
      for (var x in data) {
        responses.push(data[x])
      }
      $http.get(storeapi + 'api/days/365/events?productLineId=3&latitude=' + lat + '&longitude=' + lng + '&radius=' + range, {
        cache: 'true'
      }).success(function(data) {
        for (var x in data) {
          responses.push(data[x])
        }
        events = _.uniq(responses, 'StoreEventId')
        defferer.resolve(events)
      }).error(function(error) {});
    }).error(function(error) {});

    return defferer.promise;
  }

  this.getInternationalEvents = function() {
    var defferer = $q.defer();
    var range = 12500;
    var events = []
    var responses = [];
    $http.get(storeapi + 'api/days/365/events?productLineId=2&latitude=0&longitude=0&radius=' + range, {
      cache: 'true'
    }).success(function(data) {
      for (var x in data) {
        responses.push(data[x])
      }
      $http.get(storeapi + 'api/days/365/events?productLineId=3&latitude=0&longitude=0&radius=' + range, {
        cache: 'true'
      }).success(function(data) {
        for (var x in data) {
          responses.push(data[x])
        }
        events = _.uniq(responses, 'StoreEventId')
        events = _.remove(events, function(event) {
          return event.Country !== "United States"
        })

        //Take out duplicates
        var uniqueList = _.uniq(events, 'StoreId')
          //Group by states
        var eventsByState = _.groupBy(uniqueList, 'Country')
        var array = []
        for (var key in eventsByState) {
          var object = {
            Name: key,
            Events: eventsByState[key]
          }
          array.push(object)
        }
        defferer.resolve(array)
      }).error(function(error) {});
    }).error(function(error) {});

    return defferer.promise;
  }

  this.getInCountry = function(countryId) {
    var defferer = $q.defer();
    var events = [];
    $http.get(storeapi + 'api/days/365/events?countryid=' + countryId + '&state=', {
      cache: 'true'
    }).success(function(events) {

      //Take out duplicates
      var uniqueList = _.uniq(events, 'StoreId')
        //Group by states
      var eventsByState = _.groupBy(uniqueList, 'State')
      var array = []
      for (var key in eventsByState) {
        var object = {
          Name: key,
          Events: eventsByState[key]
        }
        array.push(object)
      }
      defferer.resolve(array);
    }).error(function(error) {});
    return defferer.promise;
  }

})
