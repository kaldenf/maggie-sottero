app.service('favorites', function($window, $rootScope, $q, $http, $localStorage) {

  var api = 'https://cms-api.maggiesottero.com';
  //https://cms-api.maggiesottero.com

  this.toggleFavoriteDress = function(dressId) {
    var deferred = $q.defer();

    //check if needs to be removed or added
    var favorited = this.checkIfFavorited(dressId);

    if ($localStorage['jwtToken'] && $localStorage['userId']) {
      //Logged in
      var token = $localStorage['jwtToken'];
      var userId = $localStorage['userId'];
      var api = 'https://cms-api.maggiesottero.com';


      if (favorited) {
        //Delete Favorite
        $http.delete(api + '/api/v1/users/' + userId + '/favorite/' + dressId, {
          headers: {
            "Authorization": "Authorization " + token
          }
        }).then(function(response) {
          //Remove from rootScope for site
          $rootScope.userFavorites = $.grep($rootScope.userFavorites, function(value) {
            return value != dressId;
          });
          $rootScope.$apply;
          deferred.resolve(false);
        }, function(error) {

        });

      } else {
        //Add Favorite
        dressId = dressId.toString();
        $http.post(api + '/api/v1/users/' + userId + '/add_favorite', {
          favorite: dressId
        }, {
          headers: {
            "Authorization": "Authorization " + token
          }
        }).then(function(response) {
          //Add to rootScope for site
          $rootScope.userFavorites.push(dressId);
          deferred.resolve(true);
        }, function(error) {

        });

      }

    } else {
      //Not Logged in
      if (favorited) {
        //Remove Favorite
        var favorites = JSON.parse($localStorage['favorites']);

        //Remove from array by value function
        Array.prototype.remove = function() {
          var what, a = arguments,
            L = a.length,
            ax;
          while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
              this.splice(ax, 1);
            }
          }
          return this;
        };
        //Remove from array
        favorites.remove(dressId);

        //Update
        $rootScope.userFavorites = favorites;
        $localStorage['favorites'] = JSON.stringify($rootScope.userFavorites);
        deferred.resolve(false);
      } else {
        //Add Favorite
        var favorites = [];
        if ($localStorage['favorites']) {
          favorites = JSON.parse($localStorage['favorites']);
        }
        favorites.push(dressId);
        $rootScope.userFavorites = favorites;
        $localStorage['favorites'] = JSON.stringify($rootScope.userFavorites);
        deferred.resolve(true);
      }
    }

    return deferred.promise;
  }

  this.addFavorite = function(dressId) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.post(api + '/api/v1/users/' + userId + '/add_favorite', {
      favorite: "" + dressId + ""
    }, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      $rootScope.userFavorites.push(dressId);
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.checkIfFavorited = function(dressId) {
    if ($.inArray(dressId, $rootScope.userFavorites) !== -1) {
      return true;
    } else {
      return false;
    }
  };

  this.removeAllFavorites = function() {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];
    var api = 'https://cms-api.maggiesottero.com';

    $http.delete(api + '/api/v1/users/' + userId + '/favorites', {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.removeFavorite = function(dressId) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];
    var api = 'https://cms-api.maggiesottero.com';

    $http.delete(api + '/api/v1/users/' + userId + '/favorite/' + dressId, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      $rootScope.userFavorites = $.grep($rootScope.userFavorites, function(value) {
        return value != dressId;
      });
      $rootScope.$apply;
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

});
