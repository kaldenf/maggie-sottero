app.service('aws', function($q) {

  this.uploadImage = function(files) {

    function randString(x) {
      var s = "";
      while (s.length < x && x > 0) {
        var r = Math.random();
        s += (r < 0.1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > 0.5 ? 97 : 65)));
      }
      return s;
    }

    var ds = _.map(files, function(file) {

      var deferred = $q.defer();

      var creds = {
        bucket: 'maggiebrides/uploads',
        access_key: 'AKIAJYSM5OXYWZNXJFHQ',
        secret_key: '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'
      }

      // Configure The S3 Object
      AWS.config.update({
        accessKeyId: creds.access_key,
        secretAccessKey: creds.secret_key
      });
      AWS.config.region = 'us-west-2';
      var bucket = new AWS.S3({
        params: {
          Bucket: creds.bucket
        }
      });

      if (file) {

        var extension = randString(10)
        var newName = extension + file.name

        var params = {
          Key: newName,
          ContentType: file.type,
          Body: file,
          ServerSideEncryption: 'AES256'
        };

        bucket.putObject(params, function(err, data) {
            if (err) {
              // There Was An Error With Your S3 Config
              alert(err.message);
              return false;
            } else {
              // Success!
              var fileName = newName.replace(/\ /g, '+');
              deferred.resolve('https://s3-us-west-2.amazonaws.com/maggiebrides/uploads/' + fileName);
            }
          })
          .on('httpUploadProgress', function(progress) {
            // Log Progress Information

          });
      } else {
        // No File Selected
        alert('No File Selected');
      }

      return deferred.promise;
    })

    return $q.all(ds)
  }

});
