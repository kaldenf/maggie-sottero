app.service('user', function($http, $q, $window, $rootScope, $localStorage) {

  var api = 'https://cms-api.maggiesottero.com';

  this.getInfo = function() {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.get(api + '/api/v1/users/' + userId, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }, {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.getAboutMe = function() {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.get(api + '/api/v1/users/' + userId + '/about_me', {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.createAboutMe = function(about_me) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.post(api + '/api/v1/users/' + userId + '/about_me', {
      country: about_me.country,
      zip: about_me.zip,
      source: about_me.source,
      name: about_me.name,
      wdate: about_me.wdate
    }, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.updateAboutMe = function(about_me) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.put(api + '/api/v1/users/' + userId + '/about_me', {
      country: about_me.country,
      zip: about_me.zip,
      source: about_me.source,
      wdate: about_me.wdate,
      name: about_me.name
    }, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }, {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.getFavorites = function() {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.get(api + '/api/v1/users/' + userId + '/favorites', {
      headers: {
        "Authorization": "Authorization " + token
      }
    }, {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.update = function(user) {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    var params = {
      email: user.email,
      your_style: user.your_style
    }
    if (user.password !== '') {
      params.password = user.password;
    }

    $http.put(api + '/api/v1/users/' + userId, params, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.changePassword = function(newPassword) {
    var deferred = $q.defer();

    $http.post('/someUrl', {
      msg: 'hello word!'
    }).then(function(response) {
      deferred.resolve(true);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.register = function(email, password) {
    var deferred = $q.defer();
    $http.post(api + '/api/v1/users', {
      email: email,
      password: password,
      your_style: 0,
      newsletter: true
    }).then(function(response) {
      $localStorage['jwtToken'] = response.data.user.token;
      $localStorage['userId'] = response.data.user.id;
      deferred.resolve(true);
    }, function(error) {
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout(function(response) {
            // Person is now logged out
          });
        }
      });
      $rootScope.$broadcast('notification', 'Registration Failed');
    });

    return deferred.promise;
  };

  this.login = function(email, password) {
    var deferred = $q.defer();
    $http.post(api + '/api/v1/users/login', {
      email: email,
      password: password
    }).then(function(response) {
      $localStorage['jwtToken'] = response.data.user.token;
      $localStorage['userId'] = response.data.user.id;
      deferred.resolve(response);
    }, function(error) {
      $rootScope.$broadcast('notification', 'Login Failed');
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout(function(response) {
            // Person is now logged out
          });
        }
      });
    });

    return deferred.promise;
  };

  this.logout = function() {
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout(function(response) {
          // Person is now logged out
        });
      }
    });
    $rootScope.userFavorites = [];
    delete $localStorage['jwtToken'];
    delete $localStorage['userId'];
  };

  this.addUserToNewsletter = function(email) {
    var deferred = $q.defer();
    $http.post(api + '/api/v1/newsletters/subscribe', {
      email: email
    }, {}).then(function(response) {
      deferred.resolve(true);
    }, function(error) {
      $rootScope.$broadcast('notification', 'Email already taken');
    });

    return deferred.promise;
  }

  this.sendContactForm = function(email, name, user_type, message) {
    var deferred = $q.defer();

    $http.post(api + '/api/v1/newsletters/contact', {
      email: email,
      name: name,
      user_type: user_type,
      message: message
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  };

  this.getLocationByIP = function() {
    var deferred = $q.defer();
    $http.get(api + '/api/v1/users/geo/info', {
      cache: 'true'
    }).then(function(response) {
      deferred.resolve(response.data);
    }, function(error) {

    });
    return deferred.promise;
  }

  this.addToNewsletter = function() {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.put(api + '/api/v1/users/' + userId + '/add_to_mailing_list', {}, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  }

  this.removeFromNewsletter = function() {
    var deferred = $q.defer();

    var token = $localStorage['jwtToken'];
    var userId = $localStorage['userId'];

    $http.put(api + '/api/v1/users/' + userId + '/remove_from_mailing_list', {}, {
      headers: {
        "Authorization": "Authorization " + token
      }
    }).then(function(response) {
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  }

  this.forgotPassword = function(email) {
    var deferred = $q.defer();

    $http.post(api + '/api/v1/users/forgot_password', {
      email: email
    }).then(function(response) {

      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  }

  this.resetPassword = function(token, password) {
    var deferred = $q.defer();

    $http.post(api + '/api/v1/users/reset_password', {
      token: token,
      password: password
    }).then(function(response) {
      $localStorage['jwtToken'] = response.data.user.token;
      $localStorage['userId'] = response.data.user.id;
      deferred.resolve(response);
    }, function(error) {

    });

    return deferred.promise;
  }

});
