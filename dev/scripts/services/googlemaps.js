app.service('googleMaps', function($http, $q) {

  this.getAddress = function(lng, lat) {
    var defferer = $q.defer();
    $http.get('//maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&sensor=true', {
      cache: 'true'
    }).success(function(data) {
      defferer.resolve(data);
    }).error(function(data) {
      //error
    })
    return defferer.promise;
  };

  this.getLocation = function(location) {
    var defferer = $q.defer();
    $http.get('//maps.googleapis.com/maps/api/geocode/json?address=' + location + '&sensor=true', {
      cache: 'true'
    }).success(function(data) {
      var location = data.results[0].geometry.location;
      defferer.resolve(location);
    }).error(function(data) {
      //error
    })
    return defferer.promise;
  };

});
