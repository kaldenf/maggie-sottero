app.directive('files', function() {
  return {
    restrict: 'AE',
    scope: {
      files: '='
    },
    link: function(scope, el, attrs){
      el.bind('change', function(event){
        scope.files = event.target.files;
        scope.$apply();
      });
    }
  };
});

app.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});

app.directive('clickOutside', function ($document) {
    return {
       restrict: 'A',
       scope: {
           clickOutside: '&'
       },
       link: function (scope, el, attr) {

           $document.on('click', function (e) {
               if (el !== e.target && !el[0].contains(e.target)) {
                    scope.$apply(function () {
                        scope.$eval(scope.clickOutside);
                    });
                }
           });
       }
    }
});
