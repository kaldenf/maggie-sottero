var app = angular.module('app', ['ngRoute', 'slick', 'ui.bootstrap']);

app.config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode({
		enabled: true
	})
	$routeProvider
		.when('/', {
			template: '<home></home>'
		})
		.when('/1', {
			templateUrl: '/pages/midgley-home.html',
			controller: 'midgleyHomeController'
		})
		.when('/about', {
			templateUrl: '/pages/aboutUs.html',
			controller: 'aboutUsController'
		})
		.when('/contact', {
			templateUrl: '/pages/contact.html',
			controller: 'contactController'
		})
		.when('/rebecca-ingram', {
			template: '<rebeccaingram></rebeccaingram>'
		})
		.when('/maggie-sottero', {
			templateUrl: '/pages/collections.html',
			controller: 'collectionsController'
		})
		.when('/maggie-sottero/category/:category', {
			templateUrl: '/pages/collections.html',
			controller: 'collectionsController'
		})
		.when('/sottero-and-midgley/category/:category', {
			templateUrl: '/pages/collections.html',
			controller: 'collectionsController'
		})
		.when('/maggie-sottero/:name/:id', {
			templateUrl: '/pages/dress.html',
			controller: 'dressController'
		})
		.when('/maggie-sottero/season/:name/:id', {
			templateUrl: '/pages/collections.html',
			controller: 'collectionsController'
		})
		.when('/sottero-and-midgley', {
			templateUrl: '/pages/collections.html',
			controller: 'collectionsController'
		})
		.when('/sottero-and-midgley/season/:name/:id', {
			templateUrl: '/pages/collections.html',
			controller: 'collectionsController'
		})
		.when('/sottero-and-midgley/:name/:id', {
			templateUrl: '/pages/dress.html',
			controller: 'dressController'
		})
		.when('/events', {
			templateUrl: '/pages/events.html',
			controller: 'eventsController'
		})
		.when('/favorites', {
			templateUrl: '/pages/favorites.html',
			controller: 'favoritesController'
		})
		.when('/bride/:id', {
			templateUrl: '/pages/maggieBride.html',
			controller: 'maggieBrideController'
		})
		.when('/brides', {
			templateUrl: '/pages/maggieBrides.html',
			controller: 'maggieBridesController'
		})
		.when('/faq/:tab?', {
			templateUrl: '/pages/faq.html',
			controller: 'faqController'
		})
		.when('/share-wedding', {
			templateUrl: '/pages/shareWedding.html',
			controller: 'shareWeddingController'

		})
		.when('/find-a-store', {
			templateUrl: '/pages/storeLocator.html',
			controller: 'storeLocatorController'
		})
		.when('/style-guide', {
			templateUrl: '/pages/guide.html',
			controller: 'styleGuideController'
		})
		.when('/sign-up', {
			templateUrl: '/pages/signup.html',
			controller: 'signupController'

		})
		.when('/log-in', {
			templateUrl: '/pages/login.html',
			controller: 'loginController'

		})
		.when('/account', {
			templateUrl: '/pages/account.html',
			controller: 'accountController'

		})
		.when('/trends', {
			templateUrl: '/pages/trendsLanding.html',
			controller: 'trendsController'
		})
		.when('/colored-wedding-dresses', {
			templateUrl: '/pages/trendsColor.html',
			controller: 'trendsController'

		})
		.when('/lace-wedding-dresses', {
			templateUrl: '/pages/trendsLace.html',
			controller: 'trendsController'

		})
		.when('/plus-size-wedding-dresses', {
			templateUrl: '/pages/trendsPlusSize.html',
			controller: 'trendsController'

		})
		.when('/sleeve-wedding-dresses', {
			templateUrl: '/pages/trendsSleeve.html',
			controller: 'trendsController'

		})
		.when('/vintage-wedding-dresses', {
			templateUrl: '/pages/trendsVintage.html',
			controller: 'trendsController'

		})
		.when('/off-the-shoulder-wedding-dresses', {
			templateUrl: '/pages/trendsShoulder.html',
			controller: 'trendsController'
		})
		.when('/quiz', {
			templateUrl: '/pages/quiz.html',
			controller: 'quizController'
		})
		.when('/forgot-password/:token', {
			templateUrl: '/pages/resetPassword.html',
			controller: 'resetPasswordController'
		})
		.when('/404', {
			templateUrl: '/partials/404.html',
			controller: 'notFoundController'
		})
		.otherwise({
			redirectTo: '/404'
		})
});

app.run(function($rootScope, $location, $timeout, $window, $http) {

	$http.defaults.cache = true

	var midgleyScrollHandler = function() {
		$rootScope.midgleyScrollPosition = $(document).scrollTop();
	}

	var maggieScrollHandler = function() {
		$rootScope.maggieScrollPosition = $(document).scrollTop();
	}

	$rootScope.$on('$routeChangeSuccess', function() {

		$rootScope.showGlobalSearch = false
		$rootScope.globalSearch = null

		$window.ga('send', 'pageview', {
			page: $location.url()
		});

		var path = $location.path(),
			absUrl = $location.absUrl(),
			virtualUrl = absUrl.substring(absUrl.indexOf(path));
		$window.dataLayer.push({
			event: 'virtualPageView',
			virtualUrl: virtualUrl
		});

		if ($location.path() == '/maggie-sottero') {
			$rootScope.midgleyScrollPosition = 0
			var position = $rootScope.maggieScrollPosition
			$timeout(function() {
				$(window).scrollTop(position);
			}, 500);
			$(document).on('scroll', maggieScrollHandler)
		}
		else {
			$(document).off('scroll', maggieScrollHandler)
		}

		if($location.path() == '/sottero-and-midgley') {
			$rootScope.maggieScrollPosition = 0
			var position = $rootScope.midgleyScrollPosition
			$timeout(function() {
				$(window).scrollTop(position);
			}, 500);
			$(document).on('scroll', midgleyScrollHandler)
		}
		else {
			$(document).off('scroll', midgleyScrollHandler)
		}

		if($location.path() == '/rebecca-ingram'){
			$rootScope.hideheader = true
		} else {
			$rootScope.hideheader = false
		}
		$rootScope.urlPath = $location.path().split('/');
		if ($location.path() !== '/') {
			$rootScope.hideFooter = false;
		}

		$('link[rel=canonical]').attr('href','https://www.maggiesottero.com' + $location.path())

		if($location.host() !== 'maggiesottero'){
			console.log($location.host())
			//$('head').append('<meta name="robots" content="noindex">');
		}
		$('#main-nav, .collections-select').removeClass('show-menu');
		$('body').removeClass('no-scroll');
		$('body').on('click', '#main-nav a', function() {
			$('#main-nav, .collections-select').removeClass('show-menu');
			$('body').removeClass('no-scroll');
		})
		$('body').on('click', '.collections-select .block button', function() {
			$('#main-nav, .collections-select').removeClass('show-menu');
			$('body').removeClass('no-scroll');
		})
		$('body').on('click', 'footer a', function() {
			$('#main-nav, .collections-select').removeClass('show-menu');
			$('body').removeClass('no-scroll');
		})

	})
})
