var gulp = require('gulp'),
	gulp_concat = require('gulp-concat'),
	gutil = require('gulp-util'),
	sass = require('gulp-sass'),
	livereload = require('gulp-livereload');

gulp.task('pages', function() {
  gulp.src('dev/pages/*.html').pipe(gulp.dest('dist/pages')).pipe(livereload());
  gulp.src('dev/partials/*.html').pipe(gulp.dest('dist/partials')).pipe(livereload());
  gulp.src('dev/index.html').pipe(gulp.dest('dist/')).pipe(livereload());
  gulp.src('dev/sitemap.xml').pipe(gulp.dest('dist/')).pipe(livereload());
});

gulp.task('styles', function(){
	gulp.src('dev/styles/styles.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('dist/css'))
	.pipe(livereload());
});

gulp.task('scripts', function(){
	return gulp.src(
		['node_modules/jquery/dist/jquery.min.js',
		'dev/libs/autogrow.min.js',
		'node_modules/jquery-ui/jquery-ui.min.js',
		'node_modules/moment/moment.js',
		'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
		'node_modules/angular/angular.min.js',
		'node_modules/lodash/lodash.min.js',
		'node_modules/angular-route/angular-route.min.js',
		'node_modules/slick-carousel/slick/slick.min.js',
		'node_modules/angularslick/dist/slick.min.js',
		'node_modules/angular-masonry/angular-masonry.js',
		'node_modules/angular-bootstrap-npm/dist/angular-bootstrap.min.js',
		'node_modules/angular-bootstrap-npm/dist/angular-bootstrap-tpls.min.js',
		'dev/libs/cycle.js',
		'dev/libs/bootstrap/bootstrap.min.js',
		'dev/scripts/animations.js',
		'dev/scripts/config.js',
		'dev/scripts/services/*.js',
		'dev/scripts/filters.js',
		'dev/scripts/directives/*.js',
		'dev/scripts/controllers/*.js'])
  	.pipe(gulp_concat('app.js'))
  	.pipe(gulp.dest('dist/'))
		.pipe(livereload());
})

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('dev/styles/**/*.scss', ['styles']);
	gulp.watch('dev/scripts/**/*.js', ['scripts']);
	gulp.watch('dev/pages/*.html', ['pages']);
	gulp.watch('dev/index.html', ['pages']);
	gulp.watch('dev/partials/*.html', ['pages']);
	gulp.watch('dev/libs/*.js', ['scripts']);
})


gulp.task('default', ['pages','styles','scripts','watch']);
